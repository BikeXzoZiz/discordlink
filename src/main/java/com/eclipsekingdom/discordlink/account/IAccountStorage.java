package com.eclipsekingdom.discordlink.account;

import java.util.Map;
import java.util.Set;
import java.util.UUID;

public interface IAccountStorage {

    long fetchAccount(UUID playerId);


    void storeAccounts(Map<UUID, Long> accounts);

    Set<UUID> fetchUnlinked();

    void storeUnlinked(Set<UUID> unlinkedPlayers);

    boolean isLinked(long discordID);

    void shutdown();

}
