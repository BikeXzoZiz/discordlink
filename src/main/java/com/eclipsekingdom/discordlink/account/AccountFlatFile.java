package com.eclipsekingdom.discordlink.account;

import com.eclipsekingdom.discordlink.DiscordLink;
import com.eclipsekingdom.discordlink.util.files.BungeeConfig;
import com.eclipsekingdom.discordlink.util.files.IConfig;
import com.eclipsekingdom.discordlink.util.files.SpigotConfig;
import com.eclipsekingdom.discordlink.util.setup.Setup;

import java.io.File;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class AccountFlatFile implements IAccountStorage {

    private File accountFile = new File("plugins/DiscordLink/Data", "accounts.yml");
    private IConfig accountConfig = DiscordLink.getSetup() == Setup.SPIGOT ? new SpigotConfig(accountFile) : new BungeeConfig(accountFile);

    private File unlinkedFile = new File("plugins/DiscordLink/Data", "unlinked.yml");
    private IConfig unlinkedConfig = DiscordLink.getSetup() == Setup.SPIGOT ? new SpigotConfig(unlinkedFile) : new BungeeConfig(unlinkedFile);


    @Override
    public long fetchAccount(UUID playerId) {
        return accountConfig.getLong(playerId.toString());
    }

    @Override
    public void storeAccounts(Map<UUID, Long> accounts) {
        for (Map.Entry<UUID, Long> entry : accounts.entrySet()) {
            accountConfig.set(entry.getKey().toString(), entry.getValue());
        }
        accountConfig.save(accountFile);
    }

    @Override
    public Set<UUID> fetchUnlinked() {
        Set<UUID> unlinked = new HashSet<>();
        for (String idString : unlinkedConfig.getRoot()) {
            try {
                unlinked.add(UUID.fromString(idString));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return unlinked;
    }

    @Override
    public void storeUnlinked(Set<UUID> unlinkedPlayers) {
        for (UUID playerID : unlinkedPlayers) {
            unlinkedConfig.set(playerID.toString(), "-");
        }
        unlinkedConfig.save(unlinkedFile);
    }

    @Override
    public boolean isLinked(long discordID) {
        for (String playerID : accountConfig.getRoot()) {
            if (accountConfig.contains(playerID)) {
                Long playerDiscord = accountConfig.getLong(playerID);
                return (playerDiscord == discordID);
            }

        }
        return false;
    }

    @Override
    public void shutdown() {
        //do nothing
    }


}
