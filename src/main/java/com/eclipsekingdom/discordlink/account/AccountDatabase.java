package com.eclipsekingdom.discordlink.account;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class AccountDatabase implements IAccountStorage {

    private static DatabaseConnection databaseConnection = DatabaseConnection.getInstance();

    public AccountDatabase() {
        initialize();
    }

    private void initialize() {
        try {
            databaseConnection.openConnection();
            databaseConnection.getConnection().createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS DLAccounts (playerID CHAR(36) NOT NULL, discordID CHAR(18) NOT NULL, PRIMARY KEY (playerID));");
            databaseConnection.getConnection().createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS DLUnlinked (playerID CHAR(36) NOT NULL, PRIMARY KEY (playerID));");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void storeAccounts(Map<UUID, Long> accounts) {
        try {
            databaseConnection.openConnection();
            Statement statement = databaseConnection.getConnection().createStatement();
            for (Map.Entry<UUID, Long> entry : accounts.entrySet()) {
                if (entry.getValue() != null) {
                    statement.executeUpdate("REPLACE INTO DLAccounts (playerID,discordID) values('" + entry.getKey() + "','" + entry.getValue() + "')");
                } else {
                    statement.executeUpdate("DELETE FROM DLAccounts WHERE playerID = '" + entry.getKey() + "'");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Set<UUID> fetchUnlinked() {
        Set<UUID> unlinkedPlayers = new HashSet<>();
        try {
            databaseConnection.openConnection();
            Statement statement = databaseConnection.getConnection().createStatement();
            ResultSet userResult = statement.executeQuery("SELECT * FROM DLUnlinked;");
            while (userResult.next()) {
                try {
                    unlinkedPlayers.add(UUID.fromString(userResult.getString("playerID")));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return unlinkedPlayers;
    }

    @Override
    public void storeUnlinked(Set<UUID> unlinkedPlayers) {
        try {
            databaseConnection.openConnection();
            Statement statement = databaseConnection.getConnection().createStatement();
            for (UUID playerID : unlinkedPlayers) {
                statement.executeUpdate("REPLACE INTO DLUnlinked (playerID) values('" + playerID + "')");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public long fetchAccount(UUID playerId) {
        try {
            databaseConnection.openConnection();
            Statement statement = databaseConnection.getConnection().createStatement();
            ResultSet userResult = statement.executeQuery("SELECT * FROM DLAccounts WHERE playerId ='" + playerId + "';");
            if (userResult.next()) return Long.valueOf(userResult.getString("discordID"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public boolean isLinked(long discordID) {
        try {
            databaseConnection.openConnection();
            Statement statement = databaseConnection.getConnection().createStatement();
            ResultSet userResult = statement.executeQuery("SELECT * FROM DLAccounts WHERE discordID='" + discordID + "';");
            return (userResult.next());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void shutdown() {
        databaseConnection.shutdown();
    }

}

