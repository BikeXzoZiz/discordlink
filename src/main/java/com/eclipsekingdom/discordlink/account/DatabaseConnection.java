package com.eclipsekingdom.discordlink.account;

import com.eclipsekingdom.discordlink.util.system.DatabaseConfig;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.Executors;

public class DatabaseConnection {

    private static DatabaseConnection databaseConnection = new DatabaseConnection();
    private static Connection connection;
    private String host;
    private int port;
    private String database;
    private String username;
    private String password;
    private boolean ssl;

    private DatabaseConnection() {
        this.host = DatabaseConfig.getHost();
        this.port = Integer.parseInt(DatabaseConfig.getPort());
        this.database = DatabaseConfig.getDatabase();
        this.username = DatabaseConfig.getUser();
        this.password = DatabaseConfig.getPass();
        this.ssl = DatabaseConfig.getSSL();
    }

    public static DatabaseConnection getInstance() {
        return databaseConnection;
    }

    public static void shutdown() {
        try {
            if (connection != null && !connection.isClosed()) connection.close();
        } catch (SQLException e) {
            //do nothing
        }
    }

    public void openConnection() throws SQLException, ClassNotFoundException {
        synchronized (this) {
            if (connection != null && !connection.isClosed()) return;
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://" + this.host + ":" + this.port + "/" + this.database + "?useSSL=" + ssl + "&autoReconnect=true&maxReconnects=108", this.username, this.password);
        }
    }


    public Connection getConnection() {
        return connection;
    }

}
