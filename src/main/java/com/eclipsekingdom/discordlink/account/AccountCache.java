package com.eclipsekingdom.discordlink.account;

import com.eclipsekingdom.discordlink.util.scheduler.Scheduler;
import com.eclipsekingdom.discordlink.util.setup.SetupUtil;
import com.eclipsekingdom.discordlink.util.system.DatabaseConfig;

import java.util.*;

public class AccountCache {

    private static HashMap<UUID, Long> playerToDiscord = new HashMap<>();
    private static HashMap<UUID, Long> unsavedAccounts = new HashMap<>();

    private static Set<UUID> unlinkedPlayers = new HashSet<>();
    private static Set<UUID> unsavedUnlinked = new HashSet<>();

    private static boolean usingDB = DatabaseConfig.isUsingDB();
    private static IAccountStorage accountStorage = usingDB ? new AccountDatabase() : new AccountFlatFile();

    public AccountCache() {
        load();
    }

    private void load() {
        unlinkedPlayers.addAll(accountStorage.fetchUnlinked());
        Collection<UUID> onlinePlayers = SetupUtil.getOnlinePlayers();
        for (UUID playerID : onlinePlayers) {
            long discordID = accountStorage.fetchAccount(playerID);
            if (discordID > 0) playerToDiscord.put(playerID, discordID);
        }
        onlinePlayers.clear();
    }

    public static void cache(UUID playerID) {
        Scheduler.runTaskAsync(() -> {
            long discordID = accountStorage.fetchAccount(playerID);
            if (discordID > 0) playerToDiscord.put(playerID, discordID);
        });
    }

    public static void forget(UUID playerID) {
        playerToDiscord.remove(playerID);
    }

    public static void shutdown() {
        accountStorage.storeAccounts(unsavedAccounts);
        accountStorage.storeUnlinked(unsavedUnlinked);
        accountStorage.shutdown();
        unsavedAccounts.clear();
        unsavedUnlinked.clear();
    }


    private static void saveAccounts() {
        Scheduler.runTaskAsync(() -> {
            accountStorage.storeAccounts(unsavedAccounts);
            unsavedAccounts.clear();
        });
    }

    private static void saveUnlinked() {
        Scheduler.runTaskAsync(() -> {
            accountStorage.storeUnlinked(unsavedUnlinked);
            unsavedUnlinked.clear();
        });
    }

    public static boolean hasDiscord(UUID playerID) {
        return playerToDiscord.containsKey(playerID);
    }

    public static long getDiscordID(UUID playerID) {
        return playerToDiscord.get(playerID);
    }

    public static boolean isLinked(long discordID) {
        return accountStorage.isLinked(discordID);
    }

    public static void registerAccount(UUID playerId, long discordID) {
        playerToDiscord.put(playerId, discordID);
        unsavedAccounts.put(playerId, discordID);
        saveAccounts();
    }

    public static void deleteAccount(UUID playerId) {
        playerToDiscord.remove(playerId);
        unsavedAccounts.put(playerId, null);
        saveAccounts();
        unlinkedPlayers.add(playerId);
        unsavedUnlinked.add(playerId);
        saveUnlinked();
    }

    public static UUID getPlayerID(long discordID) {
        for (Map.Entry<UUID, Long> entry : playerToDiscord.entrySet()) {
            if (entry.getValue() == discordID) {
                return entry.getKey();
            }
        }
        return null;
    }

    public static boolean isFirstLink(UUID playerId) {
        return !unlinkedPlayers.contains(playerId);
    }

}
