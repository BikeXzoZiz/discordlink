package com.eclipsekingdom.discordlink;

import com.eclipsekingdom.discordlink.util.setup.Setup;
import org.bukkit.plugin.java.JavaPlugin;

public class DiscordLinkSpigot extends JavaPlugin {

    @Override
    public void onEnable(){
        DiscordLink.getInstance().enable(Setup.SPIGOT, this);
    }

    @Override
    public void onDisable(){
        DiscordLink.getInstance().disable();
    }

}
