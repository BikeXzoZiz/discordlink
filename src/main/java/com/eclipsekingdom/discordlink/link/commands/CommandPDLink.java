package com.eclipsekingdom.discordlink.link.commands;

import com.eclipsekingdom.discordlink.link.LinkRequests;
import com.eclipsekingdom.discordlink.util.WrappedPlayer;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.UUID;

import static com.eclipsekingdom.discordlink.util.language.Message.*;

public class CommandPDLink extends Command {

    public CommandPDLink(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (sender instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) sender;
            UUID playerID = player.getUniqueId();
            if (LinkRequests.hasRequest(playerID)) {
                if (args.length > 0) {
                    String sub = args[0].toLowerCase();
                    if (sub.equals("confirm")) {
                        String tag = LinkRequests.completeRequest(new WrappedPlayer(playerID, player.getName()));
                        sendMessage(player, ChatColor.BLUE + SUCCESS_LINK.fromTag(tag));
                    } else if (sub.equals("deny")) {
                        sendMessage(player, ChatColor.BLUE + SUCCESS_DENY.toString());
                        LinkRequests.cancelRequest(playerID);
                    } else {
                        sendMessage(player, ChatColor.RED + WARN_FORMAT.fromFormat("/dlink {conform/deny}"));
                    }
                } else {
                    sendMessage(player, ChatColor.RED + WARN_FORMAT.fromFormat("/dlink {conform/deny}"));
                }
            } else {
                sendMessage(player, ChatColor.RED + WARN_NO_PENDING.toString());
            }
        }
    }

    private void sendMessage(ProxiedPlayer player, String message) {
        player.sendMessage(new TextComponent(message));
    }


}
