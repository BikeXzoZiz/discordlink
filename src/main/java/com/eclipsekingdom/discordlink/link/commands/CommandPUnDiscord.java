package com.eclipsekingdom.discordlink.link.commands;

import com.eclipsekingdom.discordlink.account.AccountCache;
import com.eclipsekingdom.discordlink.link.LinkExtras;
import com.eclipsekingdom.discordlink.util.WrappedPlayer;
import com.eclipsekingdom.discordlink.util.setup.BungeeUtil;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.UUID;

import static com.eclipsekingdom.discordlink.util.language.Message.SUCCESS_UNLINK;
import static com.eclipsekingdom.discordlink.util.language.Message.WARN_SELF_NOT_LINKED;

public class CommandPUnDiscord extends Command {

    public CommandPUnDiscord(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (sender instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) sender;
            UUID playerID = player.getUniqueId();
            if (AccountCache.hasDiscord(playerID)) {
                long discordID = AccountCache.getDiscordID(playerID);
                AccountCache.deleteAccount(playerID);
                LinkExtras.removeExtras(discordID, new WrappedPlayer(playerID, player.getName()));
                BungeeUtil.sendMessage(sender, ChatColor.BLUE + SUCCESS_UNLINK.toString());
            } else {
                BungeeUtil.sendMessage(sender, ChatColor.RED + WARN_SELF_NOT_LINKED.toString());
            }
        }
    }

}
