package com.eclipsekingdom.discordlink.link.commands;

import com.eclipsekingdom.discordlink.account.AccountCache;
import com.eclipsekingdom.discordlink.link.LinkExtras;
import com.eclipsekingdom.discordlink.util.WrappedPlayer;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

import static com.eclipsekingdom.discordlink.util.language.Message.SUCCESS_UNLINK;
import static com.eclipsekingdom.discordlink.util.language.Message.WARN_SELF_NOT_LINKED;

public class CommandSUnDiscord implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (sender instanceof Player) {
            Player player = (Player) sender;
            UUID playerID = player.getUniqueId();
            if (AccountCache.hasDiscord(playerID)) {
                long discordID = AccountCache.getDiscordID(playerID);
                AccountCache.deleteAccount(playerID);
                LinkExtras.removeExtras(discordID, new WrappedPlayer(playerID, player.getName()));
                player.sendMessage(ChatColor.BLUE + SUCCESS_UNLINK.toString());
            } else {
                player.sendMessage(ChatColor.RED + WARN_SELF_NOT_LINKED.toString());
            }
        }

        return false;
    }
}
