package com.eclipsekingdom.discordlink.link.commands;

import com.eclipsekingdom.discordlink.account.AccountCache;
import com.eclipsekingdom.discordlink.util.DiscordUtil;
import com.eclipsekingdom.discordlink.util.WrappedPlayer;
import com.eclipsekingdom.discordlink.util.setup.SetupUtil;
import com.eclipsekingdom.discordlink.util.system.PluginConfig;
import com.eclipsekingdom.discordlink.util.system.Version;
import net.dv8tion.jda.api.entities.User;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.UUID;

import static com.eclipsekingdom.discordlink.util.language.Message.*;

public class CommandSDiscord implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (args.length > 0) {
            String playerName = args[0];
            WrappedPlayer player = SetupUtil.getPlayer(playerName);
            if (player != null) {
                UUID playerID = player.getId();
                if (AccountCache.hasDiscord(playerID)) {
                    long discordID = AccountCache.getDiscordID(playerID);
                    User user = DiscordUtil.getUser(discordID);
                    String userName = user != null ? user.getAsTag() : "null";
                    sender.sendMessage(ChatColor.BLUE + MISC_LINK_CHECK.fromPlayerAndDiscord(player.getName(), userName));
                } else {
                    sender.sendMessage(ChatColor.RED + WARN_NOT_LINKED.fromPlayer(player.getName()));
                }
            } else {
                sender.sendMessage(ChatColor.RED + WARN_NOT_FOUND.fromPlayer(playerName));
            }
        } else {
            sender.sendMessage(ChatColor.BLUE + LABEL_DISCORD_INVITE.toString() + ":");

            if (Version.current.value >= 112) {
                TextComponent link = new TextComponent(PluginConfig.getDiscordInvite());
                link.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, PluginConfig.getDiscordInvite()));
                link.setColor(net.md_5.bungee.api.ChatColor.AQUA);
                sender.spigot().sendMessage(link);
            } else {
                sender.sendMessage(ChatColor.AQUA + PluginConfig.getDiscordInvite());
            }
        }

        return false;
    }
}
