package com.eclipsekingdom.discordlink.link.commands;

import com.eclipsekingdom.discordlink.link.LinkRequests;
import com.eclipsekingdom.discordlink.util.WrappedPlayer;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

import static com.eclipsekingdom.discordlink.util.language.Message.*;

public class CommandSDLink implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            UUID playerID = player.getUniqueId();
            if (LinkRequests.hasRequest(playerID)) {
                if (args.length > 0) {
                    String sub = args[0].toLowerCase();
                    if (sub.equals("confirm")) {
                        String tag = LinkRequests.completeRequest(new WrappedPlayer(playerID, player.getName()));
                        sendMessage(player, ChatColor.BLUE + SUCCESS_LINK.fromTag(tag));
                    } else if (sub.equals("deny")) {
                        sendMessage(player, ChatColor.BLUE + SUCCESS_DENY.toString());
                        LinkRequests.cancelRequest(playerID);
                    } else {
                        sendMessage(player, ChatColor.RED + WARN_FORMAT.fromFormat("/dlink {conform/deny}"));
                    }
                } else {
                    sendMessage(player, ChatColor.RED + WARN_FORMAT.fromFormat("/dlink {conform/deny}"));
                }
            } else {
                sendMessage(player, ChatColor.RED + WARN_NO_PENDING.toString());
            }
        }


        return false;
    }


    private void sendMessage(Player player, String message) {
        player.sendMessage(message);
    }


}
