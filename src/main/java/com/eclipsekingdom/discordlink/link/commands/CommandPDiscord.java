package com.eclipsekingdom.discordlink.link.commands;

import com.eclipsekingdom.discordlink.account.AccountCache;
import com.eclipsekingdom.discordlink.util.DiscordUtil;
import com.eclipsekingdom.discordlink.util.WrappedPlayer;
import com.eclipsekingdom.discordlink.util.setup.BungeeUtil;
import com.eclipsekingdom.discordlink.util.setup.SetupUtil;
import com.eclipsekingdom.discordlink.util.system.PluginConfig;
import net.dv8tion.jda.api.entities.User;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Command;

import java.util.UUID;

import static com.eclipsekingdom.discordlink.util.language.Message.*;

public class CommandPDiscord extends Command {

    public CommandPDiscord(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length > 0) {
            String playerName = args[0];
            WrappedPlayer player = SetupUtil.getPlayer(playerName);
            if (player != null) {
                UUID playerID = player.getId();
                if (AccountCache.hasDiscord(playerID)) {
                    long discordID = AccountCache.getDiscordID(playerID);
                    User user = DiscordUtil.getUser(discordID);
                    String userName = user != null ? user.getAsTag() : "null";
                    BungeeUtil.sendMessage(sender, ChatColor.BLUE + MISC_LINK_CHECK.fromPlayerAndDiscord(player.getName(), userName));
                } else {
                    BungeeUtil.sendMessage(sender, ChatColor.RED + WARN_NOT_LINKED.fromPlayer(player.getName()));
                }
            } else {
                BungeeUtil.sendMessage(sender, ChatColor.RED + WARN_NOT_FOUND.fromPlayer(playerName));
            }
        } else {
            BungeeUtil.sendMessage(sender, ChatColor.BLUE + LABEL_DISCORD_INVITE.toString() + ":");
            TextComponent link = new TextComponent(PluginConfig.getDiscordInvite());
            link.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, PluginConfig.getDiscordInvite()));
            link.setColor(ChatColor.AQUA);
            sender.sendMessage(link);
        }
    }

}
