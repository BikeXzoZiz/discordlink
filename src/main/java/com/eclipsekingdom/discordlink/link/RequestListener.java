package com.eclipsekingdom.discordlink.link;

import com.eclipsekingdom.discordlink.account.AccountCache;
import com.eclipsekingdom.discordlink.util.DiscordUtil;
import com.eclipsekingdom.discordlink.util.WrappedDUser;
import com.eclipsekingdom.discordlink.util.WrappedPlayer;
import com.eclipsekingdom.discordlink.util.scheduler.Scheduler;
import com.eclipsekingdom.discordlink.util.setup.SetupUtil;
import com.eclipsekingdom.discordlink.util.system.PluginConfig;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.guild.react.GenericGuildMessageReactionEvent;
import net.dv8tion.jda.api.events.message.priv.PrivateMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static com.eclipsekingdom.discordlink.util.language.Message.*;

public class RequestListener extends ListenerAdapter {

    private JDA jda;
    private boolean controlling = PluginConfig.isControlling();
    private String reaction = PluginConfig.getReaction();
    private long messageId = PluginConfig.getLinkMessageId();

    public RequestListener(JDA jda) {
        this.jda = jda;
        jda.addEventListener(this);
    }

    private Set<Long> waitingForName = new HashSet<>();

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent e) {
        User user = e.getAuthor();
        if (user.isBot() || user.isFake()) return;
        TextChannel channel = e.getChannel();
        String raw = e.getMessage().getContentRaw();
        if (raw.startsWith("!dlmsg ") && raw.length() > 7) {
            Member member = e.getMember();
            if (DiscordUtil.memberHasHigherRoleThanBot(member)) {
                try {
                    channel.deleteMessageById(e.getMessageIdLong()).queue();
                    channel.sendMessage(raw.substring(7)).queue();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        super.onGuildMessageReceived(e);
    }

    private boolean firstMsg = true;

    @Override
    public void onGenericGuildMessageReaction(GenericGuildMessageReactionEvent e) {
        User user = e.getUser();
        if (user.isBot() || user.isFake()) return;
        if (e.getMessageIdLong() == messageId) {
            if (e.getReactionEmote().getAsCodepoints().equals(reaction)) {
                long discordID = user.getIdLong();
                user.openPrivateChannel().queue(channel -> {
                    if (!waitingForName.contains(discordID)) {
                        if (!LinkRequests.hasRequest(discordID)) {
                            waitingForName.add(discordID);
                            if (controlling) channel.sendMessage(BOT_LINK_REQUEST.fromTagAndLines(user.getAsTag())).queue();
                        }
                    }
                });
                if (!e.getReaction().hasCount()) {
                    e.getChannel().addReactionById(messageId, reaction).queue();
                }
            } else {
                e.getReaction().removeReaction(user).queueAfter(1, TimeUnit.SECONDS);
            }
        }

        if (firstMsg) {
            firstMsg = false;
            try {
                Message message = e.getChannel().getHistoryAround(messageId, 3).complete().getMessageById(messageId);
                boolean otherFound = false;
                List<MessageReaction> reactionList = message.getReactions();
                for (MessageReaction react : reactionList) {
                    if (!react.getReactionEmote().getAsCodepoints().equals(reaction)) {
                        otherFound = true;
                    }
                }
                if (otherFound) {
                    message.clearReactions().queue();
                    message.addReaction(reaction).queue();
                }
            } catch (Exception ex) {
            }
        }

        super.onGenericGuildMessageReaction(e);
    }

    private Set<Long> needsResponse = new HashSet<>();

    @Override
    public void onPrivateMessageReceived(PrivateMessageReceivedEvent e) {
        PrivateChannel channel = e.getChannel();
        long channelId = channel.getIdLong();
        if (needsResponse.contains(channelId) && e.getAuthor().getIdLong() == jda.getSelfUser().getIdLong()) {
            needsResponse.remove(channelId);
        }
        User user = e.getAuthor();
        long userID = user.getIdLong();
        if (waitingForName.contains(userID)) {
            waitingForName.remove(userID);
            String firstArg = e.getMessage().getContentRaw().split(" ")[0];
            WrappedPlayer player = SetupUtil.getPlayer(firstArg);
            if (player != null) {
                UUID playerID = player.getId();
                if (!AccountCache.hasDiscord(playerID)) {
                    if (!AccountCache.isLinked(userID)) {
                        LinkRequests.registerRequest(playerID, new WrappedDUser(userID, user.getAsTag()));
                        channel.sendMessage(BOT_LINK_REQUEST_SENT.fromPlayer(player.getName())).queue();
                    } else {
                        channel.sendMessage(BOT_ALREADY_LINKED.toString()).queue();
                    }
                } else {
                    User linkedUser = DiscordUtil.getUser(AccountCache.getDiscordID(playerID));
                    if (linkedUser.getIdLong() == userID) {
                        channel.sendMessage(BOT_ALREADY_LINKED.toString()).queue();
                    } else {
                        channel.sendMessage(BOT_ANOTHER_LINKED.fromPlayerAndDiscord(player.getName(), user.getAsTag())).queue();
                    }
                }
            } else {
                if (controlling) {
                    needsResponse.add(channelId);
                    Scheduler.runTaskLater(() -> {
                        if (needsResponse.contains(channelId)) {
                            needsResponse.remove(channelId);
                            waitingForName.add(userID);
                            channel.sendMessage(BOT_USER_NOT_ONLINE.fromPlayer(firstArg)).queue();
                            channel.sendMessage(BOT_SUGGEST.toString()).queue();
                        }
                    }, 20 * 2);
                }
            }
        }

        super.onPrivateMessageReceived(e);
    }


}
