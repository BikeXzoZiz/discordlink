package com.eclipsekingdom.discordlink.link;

import com.eclipsekingdom.discordlink.DiscordLink;
import com.eclipsekingdom.discordlink.account.AccountCache;
import com.eclipsekingdom.discordlink.sync.ITroopHolder;
import com.eclipsekingdom.discordlink.sync.SyncManager;
import com.eclipsekingdom.discordlink.util.DiscordUtil;
import com.eclipsekingdom.discordlink.util.WrappedDUser;
import com.eclipsekingdom.discordlink.util.WrappedPlayer;
import com.eclipsekingdom.discordlink.util.setup.SetupUtil;
import com.eclipsekingdom.discordlink.util.system.PluginConfig;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;

import java.util.List;
import java.util.UUID;

public class LinkExtras {

    private static boolean addRole = PluginConfig.isAddVerifiedRole();
    private static boolean addGroup = PluginConfig.isAddVerifiedGroup();
    private static boolean addNick = PluginConfig.isModifyNicknames();
    private static String verifiedGroup = PluginConfig.getVerifiedGroup();
    private static Role verifiedRole;

    private static List<String> firstLinkCommands = PluginConfig.getFirstLinkCommands();
    private static List<String> linkCommands = PluginConfig.getLinkCommands();
    private static List<String> unlinkCommands = PluginConfig.getUnlinkCommands();


    public LinkExtras() {
        this.verifiedRole = DiscordUtil.getRole(PluginConfig.getVerifiedRoleID());
    }


    public static void addExtras(WrappedDUser dUser, WrappedPlayer player) {
        long discordID = dUser.getId();
        String playerName = player.getName();
        Member member = DiscordUtil.getMember(discordID);
        if (member != null) {
            if (addNick) DiscordUtil.addNickName(member, playerName);
            if (addRole) DiscordUtil.addRole(member, verifiedRole);
        }
        UUID playerID = player.getId();
        if (DiscordLink.isSyncingRoles()) {
            ITroopHolder holder = DiscordLink.getPermission();
            if (addGroup && !holder.inTroop(playerID, verifiedGroup)) holder.addTroop(playerID, verifiedGroup);
            SyncManager.coldSync(playerID);
        }
        dispatchPlayerCommands(linkCommands, playerID, playerName);
        if (AccountCache.isFirstLink(playerID)) dispatchPlayerCommands(firstLinkCommands, playerID, playerName);
    }

    private static void dispatchPlayerCommands(List<String> commands, UUID playerID, String playerName) {
        for (String command : commands) {
            SetupUtil.dispatchCommand(playerID, command.replaceAll("%player%", playerName));
        }
    }

    public static void removeExtras(long discordID, WrappedPlayer player) {
        Member member = DiscordUtil.getMember(discordID);
        if (member != null) {
            if (addNick && DiscordUtil.botHasHigherThan(member)) {
                if (player.getName().equals(member.getNickname())) {
                    member.modifyNickname(member.getUser().getName()).queue();
                }
            }
            if (addRole) DiscordUtil.removeRole(member, verifiedRole);
        }

        UUID playerID = player.getId();
        String playerName = player.getName();
        if (DiscordLink.isSyncingRoles()) {
            ITroopHolder holder = DiscordLink.getPermission();
            if (addGroup && holder.inTroop(playerID, verifiedGroup)) holder.removeTroop(playerID, verifiedGroup);
            SyncManager.strip(playerID);
        }
        dispatchPlayerCommands(unlinkCommands, playerID, playerName);
    }


}
