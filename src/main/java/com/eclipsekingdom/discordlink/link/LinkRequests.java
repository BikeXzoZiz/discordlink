package com.eclipsekingdom.discordlink.link;

import com.eclipsekingdom.discordlink.DiscordLink;
import com.eclipsekingdom.discordlink.account.AccountCache;
import com.eclipsekingdom.discordlink.util.WrappedDUser;
import com.eclipsekingdom.discordlink.util.WrappedPlayer;
import com.eclipsekingdom.discordlink.util.scheduler.Scheduler;
import com.eclipsekingdom.discordlink.util.setup.Setup;
import com.eclipsekingdom.discordlink.util.setup.SetupUtil;
import com.eclipsekingdom.discordlink.util.system.Version;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;

import java.util.*;

import static com.eclipsekingdom.discordlink.util.language.Message.*;

public class LinkRequests {

    private static Map<UUID, WrappedDUser> playerToRequester = new HashMap<>();
    private static Set<Long> requesters = new HashSet<>();

    public static boolean hasRequest(long discordID) {
        return requesters.contains(discordID);
    }

    public static boolean hasRequest(UUID playerID) {
        return playerToRequester.containsKey(playerID);
    }

    public static void registerRequest(UUID playerID, WrappedDUser dUser) {
        requesters.add(dUser.getId());
        playerToRequester.put(playerID, dUser);
        sendAlert(playerID, dUser.getTag());
        Scheduler.runTaskLater(() -> {
            requesters.remove(dUser.getId());
            playerToRequester.remove(playerID);
        }, 20 * 60);
    }

    private static void sendAlert(UUID playerID, String tag) {

        if (DiscordLink.getSetup() == Setup.BUNGEE || Version.current.value >= 112) {
            TextComponent accept = new TextComponent("[" + LABEL_CONFIRM + "]");
            accept.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/dlink confirm"));
            accept.setColor(ChatColor.GREEN);

            TextComponent deny = new TextComponent("[" + LABEL_DENY + "]");
            deny.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/dlink deny"));
            deny.setColor(ChatColor.RED);

            TextComponent base = new TextComponent(MISC_INCOMING.fromTag(tag) + " ");
            base.addExtra(accept);
            base.addExtra(new TextComponent(" "));
            base.addExtra(deny);
            base.setColor(ChatColor.BLUE);
            SetupUtil.sendTextComponent(playerID, base);
        } else {
            SetupUtil.sendMessage(playerID, "&9" + MISC_INCOMING.fromTag(tag) );
            SetupUtil.sendMessage(playerID, "&7/dlink {&aconfirm&7/§cdeny&7}");
        }

    }


    public static String completeRequest(WrappedPlayer player) {
        UUID playerID = player.getId();
        WrappedDUser dUser = playerToRequester.get(playerID);
        playerToRequester.remove(playerID);
        requesters.remove(dUser.getId());
        LinkExtras.addExtras(dUser, player);
        AccountCache.registerAccount(playerID, dUser.getId());
        return dUser.getTag();
    }

    public static void cancelRequest(UUID playerID){
        if(playerToRequester.containsKey(playerID)){
            WrappedDUser duser = playerToRequester.get(playerID);
            requesters.remove(duser.getId());
            playerToRequester.remove(playerID);
        }
    }



}
