package com.eclipsekingdom.discordlink.util;

import com.eclipsekingdom.discordlink.DiscordLink;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.User;

public class DiscordUtil {

    private static JDA jda = DiscordLink.getJDA();
    private static Guild guild;
    private static Member botMember;

    public static void init(Guild aGuild, Member aBotMember) {
        guild = aGuild;
        botMember = aBotMember;
    }

    public static boolean botHasHigherThan(Member member) {
        return !member.isOwner() && (extractHighestRole(botMember) > extractHighestRole(member));
    }


    public static boolean memberHasHigherRoleThanBot(Member member) {
        return member.isOwner() || (extractHighestRole(member) > extractHighestRole(botMember));
    }

    public static boolean botHasHigherThan(Role role) {
        return extractHighestRole(botMember) > role.getPosition();
    }

    private static int extractHighestRole(Member member) {
        int highestBotRole = 0;
        for (Role role : member.getRoles()) {
            if (role.getPosition() > highestBotRole) {
                highestBotRole = role.getPosition();
            }
        }
        return highestBotRole;
    }


    public static User getUser(long discordID) {
        try {
            return jda.getUserById(discordID);
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    public static Member getMember(long discordID) {
        try {
            User user = getUser(discordID);
            if (user != null && guild.isMember(user)) {
                return guild.getMember(user);
            }
        } catch (IllegalArgumentException e) {
        }
        return null;
    }

    public static Guild getGuild(Long guildID) {
        try {
            return jda.getGuildById(guildID);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Role getRole(long roleID) {
        try {
            return guild.getRoleById(roleID);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Role getRole(String roleID) {
        try {
            return guild.getRoleById(roleID);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Member getSelfMember(Guild guild) {
        try {
            return guild.getMember(jda.getSelfUser());
        } catch (Exception e) {
            return null;
        }
    }

    public static void addRole(Member member, Role role) {
        if (role != null && botHasHigherThan(role)) {
            guild.addRoleToMember(member, role).queue();
        }
    }

    public static void removeRole(Member member, Role role) {
        if (role != null && botHasHigherThan(role)) {
            guild.removeRoleFromMember(member, role).queue();
        }
    }

    public static void addNickName(Member member, String nick) {
        if (DiscordUtil.botHasHigherThan(member)) {
            member.modifyNickname(nick).queue();
        }
    }


}
