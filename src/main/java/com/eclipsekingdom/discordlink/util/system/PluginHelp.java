package com.eclipsekingdom.discordlink.util.system;

import com.eclipsekingdom.discordlink.util.setup.SetupUtil;

import static com.eclipsekingdom.discordlink.util.language.Message.*;

public class PluginHelp {

    public static void showHelp(Object o) {
        SetupUtil.sendMessage(o, "&6&lDiscord Link");
        SetupUtil.sendMessage(o, "&e-------&6 " + LABEL_COMMANDS + " &e-------");
        SetupUtil.sendMessage(o, "&6/discord:&r " + TEXT_DISCORD);
        SetupUtil.sendMessage(o, "&6/discord &c[" + ARG_PLAYER + "]&6:&r " + TEXT_DISCORD_CHECK);
        SetupUtil.sendMessage(o, "&6/undiscord:&r " + TEXT_UNDISCORD);
        SetupUtil.sendMessage(o, "&6/troops:&r " + TEXT_TROOPS);
    }

}
