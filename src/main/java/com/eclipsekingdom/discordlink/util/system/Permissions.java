package com.eclipsekingdom.discordlink.util.system;

import com.eclipsekingdom.discordlink.util.setup.SetupUtil;

public class Permissions {

    private static final String GLOBAL = "discordlink.*";
    private static final String TROOP = "discordlink.troops";

    public static boolean canListTroops(Object o) {
        return hasPermission(o, TROOP);
    }

    private static boolean hasPermission(Object o, String perm) {
        return SetupUtil.hasPermission(o, GLOBAL) || SetupUtil.hasPermission(o, perm);
    }

}
