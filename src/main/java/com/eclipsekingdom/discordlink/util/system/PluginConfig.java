package com.eclipsekingdom.discordlink.util.system;

import com.eclipsekingdom.discordlink.DiscordLink;
import com.eclipsekingdom.discordlink.sync.Server;
import com.eclipsekingdom.discordlink.sync.Troop;
import com.eclipsekingdom.discordlink.util.files.BungeeConfig;
import com.eclipsekingdom.discordlink.util.files.IConfig;
import com.eclipsekingdom.discordlink.util.files.SpigotConfig;
import com.eclipsekingdom.discordlink.util.setup.Setup;
import com.google.common.collect.ImmutableList;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PluginConfig {

    private static File file = new File("plugins/DiscordLink", "config.yml");
    private static IConfig config = DiscordLink.getSetup() == Setup.SPIGOT ? new SpigotConfig(file) : new BungeeConfig(file);

    private static String languageFileString = "Language";
    private static String languageFile = "en";

    private static String botNameString = "Bot";
    private static String botName = "exampleBot";

    private static String controllingString = "Controlling Bot";
    private static boolean controlling = true;

    private static String discordInviteString = "Invite";
    private static String discordInvite = "https://discordapp.com/invite/MyInvite";

    private static String guildIDString = "Guild Id";
    private static Long guildID = 100000000000000000L;

    private static String linkMessageIdString = "Link Message Id";
    private static Long linkMessageId = 100000000000000000L;

    private static String reactionString = "Reaction Emote";
    private static String reaction = "U+1f517";

    private static String verifiedRoleIDString = "Verified Role Id";
    private static Long verifiedRoleID = 100000000000000000L;

    private static String verifiedGroupString = "Verified Group Name";
    private static String verifiedGroup = "";

    private static String modifyNicknamesString = "Modify Guild Nicknames";
    private static boolean modifyNicknames = true;

    private static String firstLinkCommandsString = "Commands on First Link";
    private static List<String> firstLinkCommands = Collections.singletonList("give %player% diamond");

    private static String linkCommandsString = "Commands on Link";
    private static List<String> linkCommands = new ArrayList<>();

    private static String unlinkCommandsString = "Commands on Unlink";
    private static List<String> unlinkCommands = new ArrayList<>();

    private static String troopsString = "Troops";
    private static List<Troop> troops = new ImmutableList.Builder<Troop>()
            .add(new Troop("mod", "10000000000", Server.MINECRAFT))
            .add(new Troop("mod", "10000000000", Server.MINECRAFT))
            .add(new Troop("mod", "10000000000", Server.DISCORD))
            .build();

    public PluginConfig() {
        load();
    }

    private void load() {
        if (file.exists()) {
            try {
                languageFile = config.getString(languageFileString, languageFile);
                botName = config.getString(botNameString, botName);
                controlling = config.getBoolean(controllingString, controlling);
                discordInvite = config.getString(discordInviteString, discordInviteString);
                guildID = config.getLong(guildIDString);
                linkMessageId = config.getLong(linkMessageIdString);
                reaction = config.getString(reactionString, reaction);
                verifiedRoleID = config.getLong(verifiedRoleIDString);
                verifiedGroup = config.getString(verifiedGroupString, verifiedGroup);
                modifyNicknames = config.getBoolean(modifyNicknamesString, modifyNicknames);
                firstLinkCommands = config.getStringList(firstLinkCommandsString);
                linkCommands = config.getStringList(linkCommandsString);
                unlinkCommands = config.getStringList(unlinkCommandsString);
                List<Troop> loadedTroops = new ArrayList<>();
                if (config.contains(troopsString)) {
                    for (String troopString : config.getStringList(troopsString)) {
                        if (troopString.contains(" ==> ")) {
                            String[] parts = troopString.split(" ==> ");
                            loadedTroops.add(new Troop(parts[0], parts[1], Server.MINECRAFT));
                        } else if (troopString.contains(" <== ")) {
                            String[] parts = troopString.split(" <== ");
                            loadedTroops.add(new Troop(parts[0], parts[1], Server.DISCORD));
                        }
                    }
                }
                troops = loadedTroops;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static String getLanguageFile() {
        return languageFile;
    }

    public static String getBotName() {
        return botName;
    }

    public static boolean isControlling() {
        return controlling;
    }

    public static String getDiscordInvite() {
        return discordInvite;
    }

    public static Long getGuildID() {
        return guildID;
    }

    public static Long getLinkMessageId() {
        return linkMessageId;
    }

    public static String getReaction() {
        return reaction;
    }

    public static boolean isAddVerifiedRole() {
        return verifiedRoleID > 0;
    }

    public static Long getVerifiedRoleID() {
        return verifiedRoleID;
    }

    public static boolean isAddVerifiedGroup() {
        return !verifiedGroup.equals("");
    }

    public static String getVerifiedGroup() {
        return verifiedGroup;
    }

    public static boolean isModifyNicknames() {
        return modifyNicknames;
    }

    public static List<String> getFirstLinkCommands() {
        return firstLinkCommands;
    }

    public static List<String> getLinkCommands() {
        return linkCommands;
    }

    public static List<String> getUnlinkCommands() {
        return unlinkCommands;
    }

    public static List<Troop> getTroops() {
        return troops;
    }

}
