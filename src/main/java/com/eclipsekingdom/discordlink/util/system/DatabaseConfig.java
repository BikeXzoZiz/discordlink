package com.eclipsekingdom.discordlink.util.system;

import com.eclipsekingdom.discordlink.DiscordLink;
import com.eclipsekingdom.discordlink.util.files.BungeeConfig;
import com.eclipsekingdom.discordlink.util.files.IConfig;
import com.eclipsekingdom.discordlink.util.files.SpigotConfig;
import com.eclipsekingdom.discordlink.util.setup.Setup;

import java.io.File;

public class DatabaseConfig {

    private static File file = new File("plugins/DiscordLink", "database.yml");
    private static IConfig config = DiscordLink.getSetup() == Setup.SPIGOT ? new SpigotConfig(file) : new BungeeConfig(file);

    private static String usingString = "Use Database";
    private static boolean using = false;

    private static String hostString = "host";
    private static String host = "00.00.000.00";

    private static String portString = "port";
    private static String port = "3306";

    private static String databaseString = "database";
    private static String database = "myDatabase";

    private static String userString = "username";
    private static String user = "myUsername";

    private static String passString = "password";
    private static String pass = "myPassword";

    private static String sslString = "ssl";
    private static boolean ssl = false;

    public DatabaseConfig() {
        load();
    }

    private void load() {
        if (file.exists()) {
            try {
                using = config.getBoolean(usingString, using);
                host = config.getString(hostString, host);
                port = config.getString(portString, port);
                database = config.getString(databaseString, database);
                user = config.getString(userString, user);
                pass = config.getString(passString, pass);
                ssl = config.getBoolean(sslString, ssl);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static boolean isUsingDB() {
        return using;
    }

    public static String getHost() {
        return host;
    }

    public static String getPort() {
        return port;
    }

    public static String getDatabase() {
        return database;
    }

    public static String getUser() {
        return user;
    }

    public static String getPass() {
        return pass;
    }

    public static boolean getSSL() {
        return ssl;
    }

}
