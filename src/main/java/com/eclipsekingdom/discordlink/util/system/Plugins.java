package com.eclipsekingdom.discordlink.util.system;

import com.eclipsekingdom.discordlink.sync.permission.IPermission;
import com.eclipsekingdom.discordlink.sync.permission.PermissionPlugin;
import com.eclipsekingdom.discordlink.util.setup.SetupUtil;

import static com.eclipsekingdom.discordlink.util.language.Message.CONSOLE_PLUGIN_DETECTED;

public class Plugins {

    public String botBankNameSpace = "BotBank";
    private static boolean usingBotBank = false;

    public String placeholderNameSpace = "PlaceholderAPI";
    private static boolean usingPlaceholder = false;

    public Plugins() {
        load();
    }

    private void load() {
        if (SetupUtil.isPluginLoaded(botBankNameSpace)) {
            usingBotBank = true;
            SetupUtil.sendConsole(CONSOLE_PLUGIN_DETECTED.fromPlugin(botBankNameSpace));
        }
        if (SetupUtil.isPluginLoaded(placeholderNameSpace)) {
            usingPlaceholder = true;
            SetupUtil.sendConsole(CONSOLE_PLUGIN_DETECTED.fromPlugin(placeholderNameSpace));
        }
    }

    public static IPermission selectPermPlugin() {
        for (PermissionPlugin plugin : PermissionPlugin.values()) {
            String namespace = plugin.getNamespace();
            Object o = SetupUtil.getPlugin(namespace);
            if (o != null) {
                SetupUtil.sendConsole(CONSOLE_PLUGIN_DETECTED.fromPlugin(namespace));
                plugin.initialize(o);
                return plugin.getPermission();
            }
        }
        return null;
    }

    public static boolean isUsingBotBank() {
        return usingBotBank;
    }

    public static boolean isUsingPlaceholder() {
        return usingPlaceholder;
    }

}
