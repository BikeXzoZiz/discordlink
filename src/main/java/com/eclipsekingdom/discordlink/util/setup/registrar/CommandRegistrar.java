package com.eclipsekingdom.discordlink.util.setup.registrar;

import com.eclipsekingdom.discordlink.DiscordLink;
import com.eclipsekingdom.discordlink.util.setup.Setup;

public class CommandRegistrar {

    private IRegistrar registrar;

    public CommandRegistrar() {
        registrar = DiscordLink.getSetup() == Setup.SPIGOT ? new SpigotRegistrar() : new BungeeRegistrar();
        registrar.registerBase();
    }

    public void registerTroop() {
        registrar.registerTroop();
    }

}
