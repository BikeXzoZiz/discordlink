package com.eclipsekingdom.discordlink.util.setup;

import com.eclipsekingdom.discordlink.DiscordLink;
import com.eclipsekingdom.discordlink.util.WrappedPlayer;
import net.md_5.bungee.api.chat.TextComponent;

import java.io.InputStream;
import java.util.Collection;
import java.util.UUID;

public class SetupUtil {

    private static ISetupUtil setupUtil = DiscordLink.getSetup() == Setup.SPIGOT ? new SpigotUtil() : new BungeeUtil();

    public static void sendConsole(String message) {
        setupUtil.sendConsole(message);
    }

    public static Collection<UUID> getOnlinePlayers() {
        return setupUtil.getOnlinePlayers();
    }

    public static boolean isPluginLoaded(String namespace) {
        return setupUtil.isPluginLoaded(namespace);
    }

    public static WrappedPlayer getPlayer(String name) {
        return setupUtil.getPlayer(name);
    }

    public static void sendTextComponent(UUID playerID, TextComponent textComponent) {
        setupUtil.sendTextComponent(playerID, textComponent);
    }

    public static void sendMessage(UUID playerID, String message) {
        setupUtil.sendMessage(playerID, message);
    }

    public static InputStream getResource(String res) {
        return setupUtil.getResource(res);
    }

    public static void dispatchCommand(UUID playerID, String command) {
        setupUtil.dispatchCommand(playerID, command);
    }

    public static void sendMessage(Object o, String message) {
        setupUtil.sendMessage(o, message);
    }


    public static Object getPlugin(String namespace) {
        return setupUtil.getPlugin(namespace);
    }

    public static String getVersion(Object plugin) {
        return setupUtil.getVersion(plugin);
    }

    public static boolean hasPermission(Object o, String perm) {
        return setupUtil.hasPermission(o, perm);
    }

    public static String getVersion(){
        return setupUtil.getVersion();
    }

}
