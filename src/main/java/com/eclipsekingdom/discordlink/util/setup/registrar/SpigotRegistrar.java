package com.eclipsekingdom.discordlink.util.setup.registrar;

import com.eclipsekingdom.discordlink.CommandSDiscordLink;
import com.eclipsekingdom.discordlink.DiscordLink;
import com.eclipsekingdom.discordlink.link.commands.CommandSDLink;
import com.eclipsekingdom.discordlink.link.commands.CommandSDiscord;
import com.eclipsekingdom.discordlink.link.commands.CommandSUnDiscord;
import com.eclipsekingdom.discordlink.sync.CommandSTroops;
import org.bukkit.plugin.java.JavaPlugin;

public class SpigotRegistrar implements IRegistrar{

    @Override
    public void registerBase() {
        JavaPlugin plugin = (JavaPlugin) DiscordLink.getPlugin();
        plugin.getCommand("dlink").setExecutor(new CommandSDLink());
        plugin.getCommand("discordlink").setExecutor(new CommandSDiscordLink());
        plugin.getCommand("discord").setExecutor(new CommandSDiscord());
        plugin.getCommand("undiscord").setExecutor(new CommandSUnDiscord());
    }

    @Override
    public void registerTroop() {
        JavaPlugin plugin = (JavaPlugin) DiscordLink.getPlugin();
        plugin.getCommand("troops").setExecutor(new CommandSTroops());
    }
}
