package com.eclipsekingdom.discordlink.util.setup.registrar;

public interface IRegistrar {

    void registerBase();
    void registerTroop();

}
