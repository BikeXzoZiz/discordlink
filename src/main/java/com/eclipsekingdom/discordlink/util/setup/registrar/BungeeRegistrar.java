package com.eclipsekingdom.discordlink.util.setup.registrar;

import com.eclipsekingdom.discordlink.CommandPDiscordLink;
import com.eclipsekingdom.discordlink.DiscordLink;
import com.eclipsekingdom.discordlink.link.commands.CommandPDLink;
import com.eclipsekingdom.discordlink.link.commands.CommandPDiscord;
import com.eclipsekingdom.discordlink.link.commands.CommandPUnDiscord;
import com.eclipsekingdom.discordlink.sync.CommandPTroops;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;

public class BungeeRegistrar implements IRegistrar {

    @Override
    public void registerBase() {
        Plugin plugin = (Plugin) DiscordLink.getPlugin();
        PluginManager pm = plugin.getProxy().getPluginManager();
        pm.registerCommand(plugin, new CommandPDLink("dlink"));
        pm.registerCommand(plugin, new CommandPDiscordLink("discordlink"));
        pm.registerCommand(plugin, new CommandPDiscord("discord"));
        pm.registerCommand(plugin, new CommandPUnDiscord("undiscord"));
    }

    @Override
    public void registerTroop() {
        Plugin plugin = (Plugin) DiscordLink.getPlugin();
        PluginManager pm = plugin.getProxy().getPluginManager();
        pm.registerCommand(plugin, new CommandPTroops("troops"));
    }
}
