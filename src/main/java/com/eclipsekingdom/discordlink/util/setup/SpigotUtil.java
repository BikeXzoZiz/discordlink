package com.eclipsekingdom.discordlink.util.setup;

import com.eclipsekingdom.discordlink.DiscordLink;
import com.eclipsekingdom.discordlink.util.WrappedPlayer;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Server;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

public class SpigotUtil implements ISetupUtil {

    private JavaPlugin plugin = (JavaPlugin) DiscordLink.getPlugin();
    private Server server = plugin.getServer();
    private PluginManager pluginManager = server.getPluginManager();

    private ConsoleCommandSender consoleSender = Bukkit.getConsoleSender();


    @Override
    public void sendConsole(String message) {
        consoleSender.sendMessage("[DiscordLink] " + message);
    }


    @Override
    public Collection<UUID> getOnlinePlayers() {
        List<UUID> playerIDs = new ArrayList<>();
        for (Player player : server.getOnlinePlayers()) {
            playerIDs.add(player.getUniqueId());
        }
        return playerIDs;
    }


    @Override
    public WrappedPlayer getPlayer(String name) {
        Player player = server.getPlayer(name);
        return player != null ? new WrappedPlayer(player.getUniqueId(), player.getName()) : null;
    }

    @Override
    public boolean isPluginLoaded(String namespace) {
        Plugin plugin = pluginManager.getPlugin(namespace);
        return (plugin != null && plugin.isEnabled());
    }

    @Override
    public void sendTextComponent(UUID playerID, TextComponent textComponent) {
        Player player = server.getPlayer(playerID);
        if (player != null) {
            player.spigot().sendMessage(textComponent);
        }
    }

    @Override
    public void sendMessage(UUID playerID, String message) {
        Player player = Bukkit.getPlayer(playerID);
        if (player != null) {
            player.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
        }
    }

    @Override
    public InputStream getResource(String res) {
        return plugin.getResource(res);
    }

    @Override
    public void dispatchCommand(UUID playerID, String command) {
        Bukkit.dispatchCommand(consoleSender, command);
    }

    @Override
    public void sendMessage(Object o, String message) {
        if (o instanceof CommandSender) {
            ((CommandSender) o).sendMessage(ChatColor.translateAlternateColorCodes('&', message));
        }
    }


    @Override
    public Object getPlugin(String namespace) {
        return pluginManager.getPlugin(namespace);
    }

    @Override
    public String getVersion(Object plugin) {
        if (plugin instanceof Plugin) {
            return ((Plugin) plugin).getDescription().getVersion();
        } else {
            return "";
        }
    }

    @Override
    public boolean hasPermission(Object o, String perm) {
        if (o instanceof CommandSender) {
            return ((CommandSender) o).hasPermission(perm);
        }
        return false;
    }

    @Override
    public String getVersion() {
        return Bukkit.getVersion();
    }

}
