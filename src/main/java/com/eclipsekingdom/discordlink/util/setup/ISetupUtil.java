package com.eclipsekingdom.discordlink.util.setup;

import com.eclipsekingdom.discordlink.util.WrappedPlayer;
import net.md_5.bungee.api.chat.TextComponent;

import java.io.InputStream;
import java.util.Collection;
import java.util.UUID;

public interface ISetupUtil {

    void sendConsole(String message);

    Collection<UUID> getOnlinePlayers();

    WrappedPlayer getPlayer(String name);

    boolean isPluginLoaded(String namespace);

    void sendTextComponent(UUID playerID, TextComponent textComponent);

    void sendMessage(UUID playerID, String message);

    InputStream getResource(String res);

    void dispatchCommand(UUID playerID, String command);

    void sendMessage(Object o, String message);

    Object getPlugin(String namespace);

    String getVersion(Object plugin);

    boolean hasPermission(Object o, String perm);

    String getVersion();

}
