package com.eclipsekingdom.discordlink.util.setup;

import com.eclipsekingdom.discordlink.DiscordLink;
import com.eclipsekingdom.discordlink.util.WrappedPlayer;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

public class BungeeUtil implements ISetupUtil {

    private Plugin plugin = (Plugin) DiscordLink.getPlugin();
    private ProxyServer proxyServer = plugin.getProxy();
    private PluginManager pluginManager = proxyServer.getPluginManager();

    @Override
    public void sendConsole(String message) {
        proxyServer.broadcast(new TextComponent("[DiscordLink] " + message));
    }

    @Override
    public Collection<UUID> getOnlinePlayers() {
        List<UUID> playerIDs = new ArrayList<>();
        for (ProxiedPlayer player : proxyServer.getPlayers()) {
            playerIDs.add(player.getUniqueId());
        }
        return playerIDs;
    }

    @Override
    public WrappedPlayer getPlayer(String name) {
        ProxiedPlayer player = proxyServer.getPlayer(name);
        return player != null ? new WrappedPlayer(player.getUniqueId(), player.getName()) : null;
    }

    @Override
    public boolean isPluginLoaded(String namespace) {
        Plugin plugin = pluginManager.getPlugin(namespace);
        return (plugin != null);
    }


    @Override
    public void sendTextComponent(UUID playerID, TextComponent textComponent) {
        ProxiedPlayer player = proxyServer.getPlayer(playerID);
        if (player != null) {
            player.sendMessage(textComponent);
        }
    }

    @Override
    public void sendMessage(UUID playerID, String message) {
        ProxiedPlayer player = proxyServer.getPlayer(playerID);
        if (player != null) {
            player.sendMessage(new TextComponent(ChatColor.translateAlternateColorCodes('&', message)));
        }
    }

    public static void sendMessage(CommandSender sender, String message) {
        sender.sendMessage(new TextComponent(ChatColor.translateAlternateColorCodes('&', message)));
    }

    @Override
    public InputStream getResource(String res) {
        return plugin.getResourceAsStream(res);
    }

    @Override
    public void dispatchCommand(UUID playerID, String command) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("Command");
        out.writeUTF(command);
        proxyServer.getPlayer(playerID).getServer().getInfo().sendData("discord:link", out.toByteArray());
    }

    @Override
    public void sendMessage(Object o, String message) {
        if (o instanceof CommandSender) {
            ((CommandSender) o).sendMessage(new TextComponent(ChatColor.translateAlternateColorCodes('&', message)));
        }
    }

    @Override
    public Object getPlugin(String namespace) {
        return pluginManager.getPlugin(namespace);
    }

    @Override
    public String getVersion(Object plugin) {
        if (plugin instanceof Plugin) {
            return ((Plugin) plugin).getDescription().getVersion();
        } else {
            return "";
        }
    }


    @Override
    public boolean hasPermission(Object o, String perm) {
        if (o instanceof CommandSender) {
            return ((CommandSender) o).hasPermission(perm);
        }
        return false;
    }

    @Override
    public String getVersion() {
        return proxyServer.getVersion();
    }


}
