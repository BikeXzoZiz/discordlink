package com.eclipsekingdom.discordlink.util;

import java.util.UUID;

public class WrappedPlayer {

    private UUID id;
    private String name;

    public WrappedPlayer(UUID id, String name) {
        this.id = id;
        this.name = name;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

}
