package com.eclipsekingdom.discordlink.util.scheduler;

import com.eclipsekingdom.discordlink.DiscordLink;
import net.md_5.bungee.api.plugin.Plugin;

import java.util.concurrent.TimeUnit;

public class BungeeScheduler implements IScheduler {

    private Plugin plugin = (Plugin) DiscordLink.getPlugin();

    @Override
    public void runTask(Runnable r) {
        plugin.getProxy().getScheduler().runAsync(plugin, r);
    }

    @Override
    public void runTaskLater(Runnable r, int ticks) {
        plugin.getProxy().getScheduler().schedule(plugin, r, ticks / 20l, TimeUnit.SECONDS);
    }

    @Override
    public void runTaskLaterAsync(Runnable r, int ticks) {
        plugin.getProxy().getScheduler().schedule(plugin, r, ticks / 20l, TimeUnit.SECONDS);
    }

    @Override
    public void runTaskAsync(Runnable r) {
        runTask(r);
    }
}
