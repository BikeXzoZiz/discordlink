package com.eclipsekingdom.discordlink.util.scheduler;

import com.eclipsekingdom.discordlink.DiscordLink;
import com.eclipsekingdom.discordlink.util.setup.Setup;

public class Scheduler {

    private static IScheduler scheduler = DiscordLink.getSetup() == Setup.SPIGOT ? new SpigotScheduler() : new BungeeScheduler();

    public static void runTask(Runnable r) {
        scheduler.runTask(r);
    }

    public static void runTaskLater(Runnable r, int ticks) {
        scheduler.runTaskLater(r, ticks);
    }


    public static void runTaskAsync(Runnable r) {
        scheduler.runTaskAsync(r);
    }

    public static void runTaskLaterAsync(Runnable r, int ticks) {
        scheduler.runTaskLaterAsync(r, ticks);
    }

}
