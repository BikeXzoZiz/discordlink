package com.eclipsekingdom.discordlink.util.scheduler;

public interface IScheduler {

    void runTask(Runnable r);

    void runTaskLater(Runnable r, int ticks);

    void runTaskLaterAsync(Runnable r, int ticks);

    void runTaskAsync(Runnable r);

}
