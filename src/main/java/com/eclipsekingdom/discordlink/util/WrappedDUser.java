package com.eclipsekingdom.discordlink.util;

public class WrappedDUser {

    private long id;
    private String tag;

    public WrappedDUser(long id, String tag) {
        this.id = id;
        this.tag = tag;
    }

    public long getId() {
        return id;
    }

    public String getTag() {
        return tag;
    }

}
