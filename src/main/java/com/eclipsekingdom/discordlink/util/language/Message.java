package com.eclipsekingdom.discordlink.util.language;

public enum Message {

    LABEL_COMMANDS("Label - commands", "Commands"),
    LABEL_CONFIRM("Label - confirm", "Confirm"),
    LABEL_DENY("Label - deny", "Deny"),
    LABEL_DISCORD_INVITE("Label - discord invite", "Discord Invite"),

    TEXT_DISCORD("Text - discord", "get Discord invite link"),
    TEXT_UNDISCORD("Text - undiscord", "unlink Discord account"),
    TEXT_DISCORD_CHECK("Text - discord check", "check a players linked Discord"),
    TEXT_TROOPS("Text - troops", "list all  troops"),

    ARG_PLAYER("Arg - player", "player"),

    SUCCESS_LINK("Success - linked", "Account linked with %discordtag%"),
    SUCCESS_DENY("Success - deny", "Request denied."),
    SUCCESS_UNLINK("Success - unlink", "Discord unlinked."),

    WARN_NOT_PERMITTED("Warn - not permitted", "You do not have permission for this command."),
    WARN_NOT_LINKED("Warn - not linked", "%player% is not linked with Discord."),
    WARN_SELF_NOT_LINKED("Warn - self not linked", "You are not linked with Discord."),
    WARN_NOT_FOUND("Warn - not found", "%player% is not online."),
    WARN_NO_PENDING("Warn - no pending", "No pending link requests."),
    WARN_FORMAT("Warn - format", "Format is %format%"),

    CONSOLE_PLUGIN_DETECTED("Console - plugin detect", "%plugin% detected"),
    CONSOLE_BOT_OFFLINE("Console - bot offline", "Target bot %bot% is offline."),
    CONSOLE_BANK_MISSING("Console - missing botbank", "Bot Bank not found."),
    CONSOLE_FILE_ERROR("Console - file error", "Error saving %file%"),
    CONSOLE_INVALID_GUILD("Console - invalid guild", "Invalid guild."),
    CONSOLE_INVALID_MEMBER("Console - invalid member", "Bot not in guild."),

    BOT_LINK_REQUEST("Bot - link request", "What is you Minecraft username?"),
    BOT_LINK_REQUEST_SENT("Bot - link sent", "A confirmation message was sent to %player%"),
    BOT_USER_NOT_ONLINE("Bot - link offline", "%player% is not online."),
    BOT_ALREADY_LINKED("Bot - already linked", "Your account is already linked."),
    BOT_ANOTHER_LINKED("Bot - another linked", "%player% is already linked with %discordtag%"),
    BOT_SUGGEST("Bot - suggest", "Provide the name of an online player."),

    MISC_LINK_CHECK("Misc - link check", "%player% is linked with %discordtag%"),
    MISC_INCOMING("Misc - incoming", "Incoming Discord link request: %discordtag%"),

    ;
    private MessageSetting messageSetting;

    Message(String label, String message) {
        this.messageSetting = new MessageSetting(label, message);
    }

    public MessageSetting getMessageSetting() {
        return messageSetting;
    }

    public String get() {
        return messageSetting.getMessage();
    }

    @Override
    public String toString() {
        return get();
    }

    public String fromPlayer(String playerName) {
        return get().replaceAll("%player%", playerName);
    }

    public String fromFile(String fileName) {
        return get().replaceAll("%file%", fileName);
    }

    public String fromBot(String botName) {
        return get().replaceAll("%bot%", botName);
    }

    public String fromPlugin(String pluginName) {
        return get().replaceAll("%plugin%", pluginName);
    }

    public String fromPlayerAndDiscord(String playerName, String discordTag) {
        return get().replaceAll("%player%", playerName).replaceAll("%discordtag%", discordTag);
    }

    public String fromTag(String tag) {
        return get().replaceAll("%discordtag%", tag);
    }

    public String fromTagAndLines(String tag) {
        return get().replaceAll("%discordtag%", tag).replaceAll("<br>", "\n");
    }

    public String fromFormat(String format) {
        return get().replaceAll("%format%", format);
    }


}
