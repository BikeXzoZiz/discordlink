package com.eclipsekingdom.discordlink.util.language;

import com.eclipsekingdom.discordlink.DiscordLink;
import com.eclipsekingdom.discordlink.util.files.BungeeConfig;
import com.eclipsekingdom.discordlink.util.files.IConfig;
import com.eclipsekingdom.discordlink.util.files.SpigotConfig;
import com.eclipsekingdom.discordlink.util.setup.Setup;
import com.eclipsekingdom.discordlink.util.system.PluginConfig;

import java.io.File;

public class Language {

    private File file = new File("plugins/DiscordLink/Locale", PluginConfig.getLanguageFile() + ".yml");
    private IConfig config = DiscordLink.getSetup() == Setup.SPIGOT ? new SpigotConfig(file) : new BungeeConfig(file);

    public Language() {
        load();
    }

    private void load() {

        if (file.exists()) {
            try {
                for (Message message : Message.values()) {
                    MessageSetting setting = message.getMessageSetting();
                    if (config.contains(setting.getLabel())) {
                        setting.setMessage(config.getString(setting.getLabel(), setting.getMessage()));
                    } else {
                        config.set(setting.getLabel(), setting.getMessage());
                    }
                }
                config.save(file);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
