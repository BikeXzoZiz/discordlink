package com.eclipsekingdom.discordlink.util.files;

import java.io.File;
import java.util.Collection;
import java.util.List;

public interface IConfig {

    boolean getBoolean(String path, boolean defaultBool);

    String getString(String path, String defaultString);

    long getLong(String path);

    void set(String path, Object o);

    void save(File file);

    boolean contains(String path);

    Collection<String> getRoot();

    List<String> getStringList(String path);

}
