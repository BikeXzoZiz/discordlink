package com.eclipsekingdom.discordlink.util.files;

import com.eclipsekingdom.discordlink.util.setup.SetupUtil;
import com.google.common.collect.ImmutableList;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

public class ConfigLoader {

    public ConfigLoader() {
        load();
    }

    private ImmutableList<String> configs = new ImmutableList.Builder<String>()
            .add("config")
            .add("database")
            .build();

    private ImmutableList<String> languages = new ImmutableList.Builder<String>()
            .add("en")
            .add("ar")
            .add("es")
            .add("fr")
            .add("ru")
            .build();

    private void load() {
        try {
            for (String config : configs) {
                File target = new File("plugins/DiscordLink", config + ".yml");
                if (!target.exists()) {
                    load("Config/" + config + ".yml", target);
                }
            }
            for (String lang : languages) {
                File target = new File("plugins/DiscordLink/Locale", lang + ".yml");
                if (!target.exists()) {
                    load("Locale/" + lang + ".yml", target);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void load(String resource, File file) throws IOException {
        file.getParentFile().mkdirs();
        InputStream in = SetupUtil.getResource(resource);
        Files.copy(in, file.toPath());
        in.close();
    }


}
