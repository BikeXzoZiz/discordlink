package com.eclipsekingdom.discordlink.util.files;

import com.eclipsekingdom.discordlink.util.setup.SetupUtil;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.Collection;
import java.util.List;

import static com.eclipsekingdom.discordlink.util.language.Message.CONSOLE_FILE_ERROR;

public class SpigotConfig implements IConfig {

    private FileConfiguration config;

    public SpigotConfig(File file) {
        config = YamlConfiguration.loadConfiguration(file);
    }

    @Override
    public boolean getBoolean(String path, boolean defaultBool) {
        return config.getBoolean(path, defaultBool);
    }

    @Override
    public String getString(String path, String defaultString) {
        return config.getString(path, defaultString);
    }

    @Override
    public long getLong(String path) {
        return config.getLong(path);
    }

    @Override
    public void set(String path, Object o) {
        config.set(path, o);
    }

    @Override
    public void save(File file) {
        try {
            config.save(file);
        } catch (Exception e) {
            SetupUtil.sendConsole(CONSOLE_FILE_ERROR.fromFile(file.getName()));
        }
    }

    @Override
    public boolean contains(String path) {
        return config.contains(path);
    }

    @Override
    public Collection<String> getRoot() {
        return config.getRoot().getKeys(false);
    }

    @Override
    public List<String> getStringList(String path) {
        return config.getStringList(path);
    }

}
