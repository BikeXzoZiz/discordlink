package com.eclipsekingdom.discordlink.util;

import com.eclipsekingdom.discordlink.util.setup.SetupUtil;

import java.util.List;

public class InfoList {

    private static final String EMPTY_SLOT = "-";

    private String title;
    private List<String> items;
    private int pageItemCount;
    private String command;

    public InfoList(String title, List<String> items, int pageItemCount, String command) {
        this.title = title;
        this.items = items;
        this.pageItemCount = pageItemCount;
        this.command = command;
    }

    public void displayTo(Object sender, int page) {
        if (page < 1) {
            page = 1;
        }
        SetupUtil.sendMessage(sender, title);

        int startIndex = (page - 1) * pageItemCount;
        for (int i = startIndex; i < startIndex + pageItemCount; i++) {
            if (i < items.size()) {
                SetupUtil.sendMessage(sender, items.get(i));
            } else {
                SetupUtil.sendMessage(sender, EMPTY_SLOT);
            }
        }
        SetupUtil.sendMessage(sender, buildNextPageTip(command, page));

    }

    private static final String buildNextPageTip(String command, int page) {
        return ("&8Use &6/" + command + " " + (page + 1) + "&8 for the next page");
    }

}