package com.eclipsekingdom.discordlink.cache;

import com.eclipsekingdom.discordlink.DiscordLink;
import com.eclipsekingdom.discordlink.util.setup.Setup;

public class CacheListener {

    public static void register() {
        if (DiscordLink.getSetup() == Setup.SPIGOT) {
            new SpigotListener();
        } else {
            new BungeeListener();
        }
    }

}
