package com.eclipsekingdom.discordlink.cache;

import com.eclipsekingdom.discordlink.DiscordLink;
import com.eclipsekingdom.discordlink.account.AccountCache;
import com.eclipsekingdom.discordlink.sync.SyncManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.UUID;

public class SpigotListener implements Listener {

    public SpigotListener() {
        JavaPlugin plugin = (JavaPlugin) DiscordLink.getPlugin();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        UUID playerID = e.getPlayer().getUniqueId();
        AccountCache.cache(playerID);
        if (DiscordLink.isSyncingRoles()) SyncManager.onJoin(playerID);
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        AccountCache.forget(e.getPlayer().getUniqueId());
    }

}
