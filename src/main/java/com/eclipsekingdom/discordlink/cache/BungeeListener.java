package com.eclipsekingdom.discordlink.cache;

import com.eclipsekingdom.discordlink.DiscordLink;
import com.eclipsekingdom.discordlink.account.AccountCache;
import com.eclipsekingdom.discordlink.sync.SyncManager;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.event.EventHandler;

import java.util.UUID;

public class BungeeListener implements Listener {

    public BungeeListener() {
        Plugin plugin = (Plugin) DiscordLink.getPlugin();
        plugin.getProxy().getPluginManager().registerListener(plugin, this);
    }

    @EventHandler
    public void onPostLogin(PostLoginEvent e) {
        UUID playerID = e.getPlayer().getUniqueId();
        AccountCache.cache(playerID);
        if (DiscordLink.isSyncingRoles()) SyncManager.onJoin(playerID);
    }

    @EventHandler
    public void onQuit(PlayerDisconnectEvent e) {
        AccountCache.forget(e.getPlayer().getUniqueId());
    }

}
