package com.eclipsekingdom.discordlink;

import com.eclipsekingdom.botbank.BotBankAPI;
import com.eclipsekingdom.discordlink.account.AccountCache;
import com.eclipsekingdom.discordlink.cache.CacheListener;
import com.eclipsekingdom.discordlink.link.LinkExtras;
import com.eclipsekingdom.discordlink.link.RequestListener;
import com.eclipsekingdom.discordlink.sync.SyncManager;
import com.eclipsekingdom.discordlink.sync.TroopBank;
import com.eclipsekingdom.discordlink.sync.discord.Discord;
import com.eclipsekingdom.discordlink.sync.discord.DiscordListener;
import com.eclipsekingdom.discordlink.sync.permission.IPermission;
import com.eclipsekingdom.discordlink.util.DiscordUtil;
import com.eclipsekingdom.discordlink.util.files.ConfigLoader;
import com.eclipsekingdom.discordlink.util.language.Language;
import com.eclipsekingdom.discordlink.util.setup.Setup;
import com.eclipsekingdom.discordlink.util.setup.SetupUtil;
import com.eclipsekingdom.discordlink.util.setup.registrar.CommandRegistrar;
import com.eclipsekingdom.discordlink.util.system.DatabaseConfig;
import com.eclipsekingdom.discordlink.util.system.PluginConfig;
import com.eclipsekingdom.discordlink.util.system.Plugins;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;

import static com.eclipsekingdom.discordlink.util.language.Message.*;

public final class DiscordLink {

    private static DiscordLink discordLink = new DiscordLink();

    private static Setup setup;
    private static Object plugin;
    private static JDA jda;
    private static boolean enabled = false;
    private static IPermission permission;

    private DiscordLink() {
    }

    public void enable(Setup setup, Object plugin) {
        this.setup = setup;
        this.plugin = plugin;

        new ConfigLoader();
        new PluginConfig();
        new DatabaseConfig();
        new Language();

        new Plugins();

        if (Plugins.isUsingBotBank()) {
            String botName = PluginConfig.getBotName();
            BotBankAPI botBankAPI = BotBankAPI.getInstance();
            if (botBankAPI.isBotOnline(botName)) {
                jda = botBankAPI.getJDA(botName);
                if (jda != null) {
                    OnlineStatus status = PluginConfig.isControlling() ? OnlineStatus.ONLINE : null;
                    jda.getPresence().setStatus(status);
                    Guild guild = DiscordUtil.getGuild(PluginConfig.getGuildID());
                    if (guild != null) {
                        Member botMember = DiscordUtil.getSelfMember(guild);
                        if (botMember != null) {
                            DiscordUtil.init(guild, botMember);
                            new AccountCache();
                            CommandRegistrar registrar = new CommandRegistrar();
                            if (Plugins.isUsingPlaceholder() && setup == Setup.SPIGOT) new Placeholder().register();
                            CacheListener.register();
                            new RequestListener(jda);
                            new LinkExtras();
                            permission = Plugins.selectPermPlugin();
                            if (permission != null) {
                                registrar.registerTroop();
                                new TroopBank();
                                Discord discord = new Discord();
                                new SyncManager();
                                SyncManager.initialize(permission, discord);
                                new DiscordListener(jda);
                            }
                            enabled = true;
                        } else {
                            SetupUtil.sendConsole(CONSOLE_INVALID_MEMBER.toString());
                        }
                    } else {
                        SetupUtil.sendConsole(CONSOLE_INVALID_GUILD.toString());
                    }
                } else {
                    SetupUtil.sendConsole(CONSOLE_BOT_OFFLINE.fromBot(botName));
                }
            } else {
                SetupUtil.sendConsole(CONSOLE_BOT_OFFLINE.fromBot(botName));
            }
        } else {
            SetupUtil.sendConsole(CONSOLE_BANK_MISSING.toString());
        }
    }

    public void disable() {
        if (enabled) AccountCache.shutdown();
        if (jda != null && PluginConfig.isControlling()) jda.getPresence().setStatus(null);
    }

    public static Setup getSetup() {
        return setup;
    }

    public static DiscordLink getInstance() {
        return discordLink;
    }

    public static Object getPlugin() {
        return plugin;
    }

    public static JDA getJDA() {
        return jda;
    }

    public static boolean isSyncingRoles() {
        return permission != null;
    }

    public static IPermission getPermission() {
        return permission;
    }

}
