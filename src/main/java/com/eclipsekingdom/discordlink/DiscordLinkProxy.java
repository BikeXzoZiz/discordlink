package com.eclipsekingdom.discordlink;

import com.eclipsekingdom.discordlink.util.setup.Setup;
import net.md_5.bungee.api.plugin.Plugin;

public class DiscordLinkProxy extends Plugin {

    @Override
    public void onEnable() {
        DiscordLink.getInstance().enable(Setup.BUNGEE, this);
        getProxy().registerChannel("discord:link");
    }

    @Override
    public void onDisable() {
        DiscordLink.getInstance().disable();
    }

}

