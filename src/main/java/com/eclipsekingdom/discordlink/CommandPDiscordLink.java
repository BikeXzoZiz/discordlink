package com.eclipsekingdom.discordlink;

import com.eclipsekingdom.discordlink.util.system.PluginHelp;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

public class CommandPDiscordLink extends Command {

    public CommandPDiscordLink(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] strings) {
        PluginHelp.showHelp(sender);
    }

}
