package com.eclipsekingdom.discordlink;

import com.eclipsekingdom.discordlink.account.AccountCache;
import com.eclipsekingdom.discordlink.util.DiscordUtil;
import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import net.dv8tion.jda.api.entities.User;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.UUID;

public class Placeholder extends PlaceholderExpansion {

    private JavaPlugin plugin;

    public Placeholder() {
        this.plugin = (JavaPlugin) DiscordLink.getPlugin();
    }

    @Override
    public boolean persist() {
        return true;
    }

    @Override
    public boolean canRegister() {
        return true;
    }

    @Override
    public String getAuthor() {
        return "sword7";
    }

    @Override
    public String getIdentifier() {
        return "discordlink";
    }

    @Override
    public String getVersion() {
        return plugin.getDescription().getVersion();
    }

    @Override
    public String onRequest(OfflinePlayer player, String identifier) {
        if (player == null) return "";
        if (identifier.equals("discordtag")) {
            return getTagFromID(player.getUniqueId());
        } else {
            return "";
        }
    }

    @Override
    public String onPlaceholderRequest(Player player, String identifier) {
        if (player == null) return "";
        if (identifier.equals("discordtag")) {
            return getTagFromID(player.getUniqueId());
        } else {
            return "";
        }
    }

    private String getTagFromID(UUID playerID) {
        Long discordID = AccountCache.getDiscordID(playerID);
        if (discordID != null) {
            User user = DiscordUtil.getUser(discordID);
            if (user != null) {
                return user.getAsTag();
            }
        }
        return "";
    }


}
