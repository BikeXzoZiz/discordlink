package com.eclipsekingdom.discordlink.sync.permission;

import com.eclipsekingdom.discordlink.sync.Troop;
import com.eclipsekingdom.discordlink.sync.TroopBank;
import com.eclipsekingdom.discordlink.sync.permission.listener.Lucky5Listener;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.LuckPermsProvider;
import net.luckperms.api.model.user.User;
import net.luckperms.api.node.Node;
import net.luckperms.api.node.NodeEqualityPredicate;
import net.luckperms.api.util.Tristate;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Permission_LuckPerm5 extends Permission {

    private static LuckPerms luckPermAPI = LuckPermsProvider.get();

    public Permission_LuckPerm5() {
        super(PermissionPlugin.LUCKY.getNamespace());
        new Lucky5Listener(luckPermAPI.getEventBus());
    }

    @Override
    public List<Troop> getTroops(UUID playerID) {
        List<Troop> troops = new ArrayList<>();
        for (Troop troop : TroopBank.getMinecraftSourced()) {
            if (inTroop(playerID, troop.getGroup())) {
                troops.add(troop);
            }
        }
        return troops;
    }

    @Override
    public boolean inTroop(UUID playerID, String troop) {
        User user = luckPermAPI.getUserManager().getUser(playerID);
        Node node = luckPermAPI.getNodeBuilderRegistry().forInheritance().group(troop.toLowerCase()).build();
        Node tempNode = luckPermAPI.getNodeBuilderRegistry().forInheritance().group(troop.toLowerCase()).expiry(10L).build();
        return user.data().contains(node, Node::equals) == Tristate.TRUE || user.data().contains(tempNode, NodeEqualityPredicate.IGNORE_EXPIRY_TIME) == Tristate.TRUE;
    }


    @Override
    public boolean userExists(UUID playerID) {
        return luckPermAPI.getUserManager().getUser(playerID) != null;
    }

    @Override
    public void removeTroop(UUID playerID, String troop) {
        User user = luckPermAPI.getUserManager().getUser(playerID);
        Node node = luckPermAPI.getNodeBuilderRegistry().forInheritance().group(troop.toLowerCase()).build();
        if (user.data().contains(node, Node::equals) == Tristate.TRUE) {
            user.data().remove(node);
            luckPermAPI.getUserManager().saveUser(user);
        }
    }

    @Override
    public void addTroop(UUID playerID, String troop) {
        User user = luckPermAPI.getUserManager().getUser(playerID);
        Node node = luckPermAPI.getNodeBuilderRegistry().forInheritance().group(troop.toLowerCase()).build();
        if (user.data().contains(node, Node::equals) != Tristate.TRUE) {
            user.data().add(node);
            luckPermAPI.getUserManager().saveUser(user);
        }
    }

    @Override
    public boolean groupExists(String groupName) {
        return luckPermAPI.getGroupManager().getGroup(groupName.toLowerCase()) != null;
    }
}
