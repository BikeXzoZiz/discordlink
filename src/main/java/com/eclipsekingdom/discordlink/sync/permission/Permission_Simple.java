package com.eclipsekingdom.discordlink.sync.permission;

import com.eclipsekingdom.discordlink.sync.Troop;
import com.eclipsekingdom.discordlink.sync.TroopBank;
import com.eclipsekingdom.discordlink.sync.permission.listener.SimpleListener;
import com.eclipsekingdom.simpleperms.group.Group;
import com.eclipsekingdom.simpleperms.group.GroupCache;
import com.eclipsekingdom.simpleperms.user.UserCache;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Permission_Simple extends Permission {

    public Permission_Simple() {
        super(PermissionPlugin.SIMPLE_PERMS.getNamespace());
        new SimpleListener();
    }

    @Override
    public boolean groupExists(String groupName) {
        return GroupCache.hasGroup(groupName);
    }

    @Override
    public List<Troop> getTroops(UUID playerId) {
        List<Troop> troops = new ArrayList<>();
        for (Troop troop : TroopBank.getMinecraftSourced()) {
            if (inTroop(playerId, troop.getGroup())) {
                troops.add(troop);
            }
        }
        return troops;
    }

    @Override
    public boolean userExists(UUID playerId) {
        return Bukkit.getPlayer(playerId) != null;
    }

    @Override
    public void removeTroop(UUID playerId, String troop) {
        Group group = GroupCache.getGroup(troop);
        if (group != null && group.isMember(playerId)) {
            group.removeMember(playerId);
        }
    }

    @Override
    public void addTroop(UUID playerId, String troop) {
        if (!UserCache.hasUser(playerId)) {
            Player player = Bukkit.getPlayer(playerId);
            UserCache.register(player);
        }
        Group group = GroupCache.getGroup(troop);
        if (group != null && !group.isMember(playerId)) {
            group.addMember(playerId);
        }
    }

    @Override
    public boolean inTroop(UUID playerId, String troop) {
        if (GroupCache.hasGroup(troop)) {
            return GroupCache.getGroup(troop).isMember(playerId);
        } else {
            return false;
        }
    }


}
