package com.eclipsekingdom.discordlink.sync.permission.listener;

import com.eclipsekingdom.discordlink.sync.Server;
import com.eclipsekingdom.discordlink.sync.SyncManager;
import net.luckperms.api.event.EventBus;
import net.luckperms.api.event.group.GroupCreateEvent;
import net.luckperms.api.event.group.GroupDeleteEvent;
import net.luckperms.api.event.user.UserDataRecalculateEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class Lucky5Listener {

    public Lucky5Listener(EventBus eventBus) {
        eventBus.subscribe(UserDataRecalculateEvent.class, this::onCalculateData);
    }

    private void onCalculateData(UserDataRecalculateEvent e) {
        SyncManager.sync(e.getUser().getUniqueId(), Server.MINECRAFT);

    }

}
