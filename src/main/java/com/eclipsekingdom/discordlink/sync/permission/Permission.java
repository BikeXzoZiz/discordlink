package com.eclipsekingdom.discordlink.sync.permission;

public abstract class Permission implements IPermission {

    private String namespace;

    public Permission(String namespace) {
        this.namespace = namespace;
    }

    @Override
    public String getNamespace() {
        return namespace;
    }

}
