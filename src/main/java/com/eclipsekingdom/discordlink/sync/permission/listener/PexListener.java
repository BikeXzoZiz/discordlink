package com.eclipsekingdom.discordlink.sync.permission.listener;

import com.eclipsekingdom.discordlink.DiscordLink;
import com.eclipsekingdom.discordlink.sync.Server;
import com.eclipsekingdom.discordlink.sync.SyncManager;
import com.eclipsekingdom.discordlink.sync.permission.Permission_Pex;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import ru.tehkode.permissions.events.PermissionEntityEvent;

import java.util.UUID;

public class PexListener implements Listener {

    public PexListener() {
        JavaPlugin plugin = (JavaPlugin) DiscordLink.getPlugin();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPermission(PermissionEntityEvent e) {
        try {
            SyncManager.sync(UUID.fromString(e.getEntityIdentifier()), Server.MINECRAFT);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
