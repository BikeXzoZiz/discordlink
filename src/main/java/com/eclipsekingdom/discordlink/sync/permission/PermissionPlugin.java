package com.eclipsekingdom.discordlink.sync.permission;

import com.eclipsekingdom.discordlink.DiscordLink;
import com.eclipsekingdom.discordlink.util.setup.Setup;
import com.eclipsekingdom.discordlink.util.setup.SetupUtil;

public enum PermissionPlugin {

    LUCKY("LuckPerms"),
    PEX("PermissionsEx"),
    GROUP_MANAGER("GroupManager"),
    ULTRA_PERMS("UltraPermissions"),
    SIMPLE_PERMS("SimplePerms"),
    NONE(""),
    ;

    private String namespace;
    private IPermission permission;

    PermissionPlugin(String namespace) {
        this.namespace = namespace;
    }

    public void initialize(Object o) {
        this.permission = selectPermission(o);
    }

    public String getNamespace() {
        return namespace;
    }

    private IPermission selectPermission(Object o) {
        if (DiscordLink.getSetup() == Setup.SPIGOT) {
            switch (this) {
                case LUCKY:
                    return selectLuckVersion(o);
                case PEX:
                    return new Permission_Pex();
                case ULTRA_PERMS:
                    return new Permission_Ultra();
                case GROUP_MANAGER:
                    return new Permission_GroupManager(o);
                case SIMPLE_PERMS:
                    return new Permission_Simple();
                default:
                    return new Permission_Unknown();
            }
        } else {
            switch (this) {
                case LUCKY:
                    return selectLuckVersion(o);
                case ULTRA_PERMS:
                    return new Permission_BungeeUltra();
                default:
                    return new Permission_Unknown();
            }
        }
    }

    public IPermission getPermission() {
        return permission;
    }


    private IPermission selectLuckVersion(Object o) {
        String versionString = SetupUtil.getVersion(o);
        String[] parts = versionString.split("\\.");
        double totalValue = 0;
        double valueMult = 100;
        for (String part : parts) {
            double partValue = 0;
            try {
                partValue = Integer.parseInt(part) * valueMult;
            } catch (Exception e) {
                //do nothing
            }
            totalValue += partValue;
            valueMult *= 0.1;
        }

        return totalValue >= 500 ? new Permission_LuckPerm5() : new Permission_LuckPerm4();
    }
}
