package com.eclipsekingdom.discordlink.sync.permission;

import com.eclipsekingdom.discordlink.sync.Troop;
import com.eclipsekingdom.discordlink.sync.TroopBank;
import com.eclipsekingdom.discordlink.sync.permission.listener.Lucky4Listener;
import me.lucko.luckperms.LuckPerms;
import me.lucko.luckperms.api.LuckPermsApi;
import me.lucko.luckperms.api.Node;
import me.lucko.luckperms.api.Tristate;
import me.lucko.luckperms.api.User;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Permission_LuckPerm4 extends Permission {

    private static LuckPermsApi luckPermAPI = LuckPerms.getApi();

    public Permission_LuckPerm4() {
        super(PermissionPlugin.LUCKY.getNamespace());
        new Lucky4Listener(luckPermAPI.getEventBus());
    }

    @Override
    public List<Troop> getTroops(UUID playerId) {
        List<Troop> troops = new ArrayList<>();
        for (Troop troop : TroopBank.getMinecraftSourced()) {
            if (inTroop(playerId, troop.getGroup())) {
                troops.add(troop);
            }
        }
        return troops;
    }

    @Override
    public boolean inTroop(UUID playerId, String troop) {
        User user = luckPermAPI.getUser(playerId);
        Node groupNode = luckPermAPI.getNodeFactory().makeGroupNode(troop.toLowerCase()).build();
        Node tempGroupNode = luckPermAPI.getNodeFactory().makeGroupNode(troop.toLowerCase()).setExpiry(10L).build();
        return user.hasPermission(groupNode) == Tristate.TRUE || user.hasPermission(tempGroupNode) == Tristate.TRUE;
    }


    @Override
    public boolean userExists(UUID playerId) {
        return luckPermAPI.getUser(playerId) != null;
    }

    @Override
    public void removeTroop(UUID playerId, String troop) {
        User user = luckPermAPI.getUser(playerId);
        Node node = luckPermAPI.getNodeFactory().makeGroupNode(troop.toLowerCase()).build();
        if (user.hasPermission(node) == Tristate.TRUE) {
            user.unsetPermission(node);
            luckPermAPI.getUserManager().saveUser(user);
        }
    }

    @Override
    public void addTroop(UUID playerId, String troop) {
        User user = luckPermAPI.getUser(playerId);
        Node node = luckPermAPI.getNodeFactory().makeGroupNode(troop.toLowerCase()).build();
        if (user.hasPermission(node) != Tristate.TRUE) {
            user.setPermission(node);
            luckPermAPI.getUserManager().saveUser(user);
        }
    }

    @Override
    public boolean groupExists(String groupName) {
        return luckPermAPI.getGroup(groupName.toLowerCase()) != null;
    }
}
