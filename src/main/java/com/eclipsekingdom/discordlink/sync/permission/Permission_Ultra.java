package com.eclipsekingdom.discordlink.sync.permission;

import com.eclipsekingdom.discordlink.sync.Troop;
import com.eclipsekingdom.discordlink.sync.TroopBank;
import me.TechsCode.UltraPermissions.UltraPermissions;
import me.TechsCode.UltraPermissions.UltraPermissionsAPI;
import me.TechsCode.UltraPermissions.storage.objects.Group;
import me.TechsCode.UltraPermissions.storage.objects.User;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Permission_Ultra extends Permission {

    private static UltraPermissionsAPI ultraPermissionsAPI = UltraPermissions.getAPI();

    public Permission_Ultra() {
        super(PermissionPlugin.ULTRA_PERMS.getNamespace());
    }

    @Override
    public List<Troop> getTroops(UUID playerId) {
        List<Troop> troops = new ArrayList<>();
        for (Troop troop : TroopBank.getMinecraftSourced()) {
            if (inTroop(playerId, troop.getGroup())) {
                troops.add(troop);
            }
        }
        return troops;
    }

    @Override
    public boolean inTroop(UUID playerId, String troop) {
        User user = ultraPermissionsAPI.getUsers().uuid(playerId);
        Group group = ultraPermissionsAPI.getGroups().name(troop);
        return group != null && user.isInGroup(group);
    }


    @Override
    public boolean userExists(UUID playerId) {
        return ultraPermissionsAPI.getUsers().uuid(playerId) != null;
    }

    @Override
    public void removeTroop(UUID playerId, String troop) {
        User user = ultraPermissionsAPI.getUsers().uuid(playerId);
        Group group = ultraPermissionsAPI.getGroups().name(troop);
        if (group != null && user.isInGroup(group)) {
            user.removeGroup(group);
            user.save();
        }
    }

    @Override
    public void addTroop(UUID playerId, String troop) {
        User user = ultraPermissionsAPI.getUsers().uuid(playerId);
        Group group = ultraPermissionsAPI.getGroups().name(troop);
        if (group != null && !user.isInGroup(group)) {
            user.addGroup(group);
            user.save();
        }
    }

    @Override
    public boolean groupExists(String groupName) {
        return ultraPermissionsAPI.getGroups().name(groupName) != null;
    }
}