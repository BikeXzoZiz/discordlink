package com.eclipsekingdom.discordlink.sync.permission;

import com.eclipsekingdom.discordlink.sync.ITroopHolder;

public interface IPermission extends ITroopHolder {

    String getNamespace();

    boolean groupExists(String groupName);

}
