package com.eclipsekingdom.discordlink.sync.permission;

import com.eclipsekingdom.discordlink.sync.Troop;
import com.eclipsekingdom.discordlink.sync.TroopBank;
import com.eclipsekingdom.discordlink.sync.permission.listener.GroupManagerListener;
import org.anjocaido.groupmanager.GroupManager;
import org.anjocaido.groupmanager.data.Group;
import org.anjocaido.groupmanager.data.User;
import org.anjocaido.groupmanager.dataholder.OverloadedWorldHolder;
import org.anjocaido.groupmanager.permissions.AnjoPermissionsHandler;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Permission_GroupManager extends Permission {

    private GroupManager groupManager;

    public Permission_GroupManager(Object o) {
        super(PermissionPlugin.GROUP_MANAGER.getNamespace());
        Plugin plugin = (Plugin) o;
        groupManager = (GroupManager) plugin;
        new GroupManagerListener();
    }

    @Override
    public List<Troop> getTroops(UUID playerId) {
        List<Troop> troops = new ArrayList<>();
        for (Troop troop : TroopBank.getMinecraftSourced()) {
            if (inTroop(playerId, troop.getGroup())) {
                troops.add(troop);
            }
        }
        return troops;
    }

    @Override
    public boolean inTroop(UUID playerId, String troop) {
        String playerName = Bukkit.getPlayer(playerId).getName();
        AnjoPermissionsHandler handler = groupManager.getWorldsHolder().getWorldPermissionsByPlayerName(playerName);
        if (handler != null) {
            return handler.inGroup(playerName, troop);
        } else {
            return false;
        }
    }

    @Override
    public boolean userExists(UUID playerId) {
        Player player = Bukkit.getPlayer(playerId);
        if (player != null) {
            String playerName = player.getName();
            OverloadedWorldHolder overloadedWorldHolder = groupManager.getWorldsHolder().getWorldDataByPlayerName(playerName);
            if (overloadedWorldHolder != null) {
                User user = overloadedWorldHolder.getUser(playerName);
                return user != null;
            }
        }
        return false;
    }

    @Override
    public void addTroop(UUID playerId, String troop) {
        String playerName = Bukkit.getPlayer(playerId).getName();
        OverloadedWorldHolder owh = groupManager.getWorldsHolder().getWorldData(playerName);
        User user = owh.getUser(playerName);
        Group group = owh.getGroup(troop);
        if (group != null) {
            if (user.getGroup().equals(owh.getDefaultGroup())) {
                user.setGroup(group);
            } else if (group.getInherits().contains(user.getGroup().getName())) {
                user.setGroup(group);
            } else {
                user.addSubGroup(group);
            }
            Player p = Bukkit.getPlayer(playerName);
            if (p != null) {
                GroupManager.BukkitPermissions.updatePermissions(p);
            }
        }
    }

    @Override
    public void removeTroop(UUID playerId, String troop) {
        String playerName = Bukkit.getPlayer(playerId).getName();
        OverloadedWorldHolder owh = groupManager.getWorldsHolder().getWorldDataByPlayerName(playerName);
        User user = owh.getUser(playerName);

        boolean success = false;
        if (user.getGroup().getName().equalsIgnoreCase(troop)) {
            user.setGroup(owh.getDefaultGroup());
            success = true;
        } else {
            Group group = owh.getGroup(troop);
            if (group != null) {
                success = user.removeSubGroup(group);
            }
        }
        if (success) {
            Player p = Bukkit.getPlayer(playerName);
            if (p != null) {
                GroupManager.BukkitPermissions.updatePermissions(p);
            }
        }
    }

    @Override
    public boolean groupExists(String groupName) {
        OverloadedWorldHolder owh = groupManager.getWorldsHolder().getDefaultWorld();
        Group group = owh.getGroup(groupName);
        return group != null;
    }
}
