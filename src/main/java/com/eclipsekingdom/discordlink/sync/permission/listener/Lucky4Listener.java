package com.eclipsekingdom.discordlink.sync.permission.listener;

import com.eclipsekingdom.discordlink.sync.Server;
import com.eclipsekingdom.discordlink.sync.SyncManager;
import com.eclipsekingdom.discordlink.sync.permission.Permission_LuckPerm4;
import me.lucko.luckperms.api.event.EventBus;
import me.lucko.luckperms.api.event.group.GroupCreateEvent;
import me.lucko.luckperms.api.event.group.GroupDeleteEvent;
import me.lucko.luckperms.api.event.user.UserDataRecalculateEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class Lucky4Listener {


    public Lucky4Listener(EventBus eventBus) {
        eventBus.subscribe(UserDataRecalculateEvent.class, this::onCalculateData);
    }

    private void onCalculateData(UserDataRecalculateEvent e) {
        SyncManager.sync(e.getUser().getUuid(), Server.MINECRAFT);

    }


}
