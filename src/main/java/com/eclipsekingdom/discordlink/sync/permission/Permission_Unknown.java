package com.eclipsekingdom.discordlink.sync.permission;

import com.eclipsekingdom.discordlink.sync.Troop;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class Permission_Unknown implements IPermission {


    @Override
    public String getNamespace() {
        return "";
    }

    @Override
    public boolean groupExists(String groupName) {
        return false;
    }

    @Override
    public List<Troop> getTroops(UUID playerId) {
        return Collections.emptyList();
    }

    @Override
    public boolean userExists(UUID playerId) {
        return false;
    }

    @Override
    public void addTroop(UUID playerId, String troop) {

    }

    @Override
    public void removeTroop(UUID playerId, String troop) {

    }

    @Override
    public boolean inTroop(UUID playerId, String troop) {
        return false;
    }
}
