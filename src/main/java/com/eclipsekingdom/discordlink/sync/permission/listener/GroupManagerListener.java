package com.eclipsekingdom.discordlink.sync.permission.listener;

import com.eclipsekingdom.discordlink.DiscordLink;
import com.eclipsekingdom.discordlink.sync.Server;
import com.eclipsekingdom.discordlink.sync.SyncManager;
import com.eclipsekingdom.discordlink.sync.permission.Permission_GroupManager;
import org.anjocaido.groupmanager.events.GMGroupEvent;
import org.anjocaido.groupmanager.events.GMUserEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.UUID;

public class GroupManagerListener implements Listener {

    public GroupManagerListener() {
        JavaPlugin plugin = (JavaPlugin) DiscordLink.getPlugin();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPermission(GMUserEvent e) {
        try {
            GMUserEvent.Action action = e.getAction();
            if (action == GMUserEvent.Action.USER_GROUP_CHANGED || action == GMUserEvent.Action.USER_SUBGROUP_CHANGED) {
                UUID playerID = UUID.fromString(e.getUser().getUUID());
                SyncManager.sync(playerID, Server.MINECRAFT);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
