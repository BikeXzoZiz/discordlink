package com.eclipsekingdom.discordlink.sync.permission.listener;

import com.eclipsekingdom.discordlink.DiscordLink;
import com.eclipsekingdom.discordlink.sync.Server;
import com.eclipsekingdom.discordlink.sync.SyncManager;
import com.eclipsekingdom.discordlink.sync.permission.Permission_Simple;
import com.eclipsekingdom.simpleperms.events.GroupAddMemberEvent;
import com.eclipsekingdom.simpleperms.events.GroupCreateEvent;
import com.eclipsekingdom.simpleperms.events.GroupDeleteEvent;
import com.eclipsekingdom.simpleperms.events.GroupRemoveMemberEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public class SimpleListener implements Listener {

    public SimpleListener() {
        JavaPlugin plugin = (JavaPlugin) DiscordLink.getPlugin();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onGroupAdd(GroupAddMemberEvent e) {
        SyncManager.sync(e.getUserID(), Server.MINECRAFT);
    }

    @EventHandler
    public void onGroupRem(GroupRemoveMemberEvent e) {
        SyncManager.sync(e.getUserID(), Server.MINECRAFT);
    }


}
