package com.eclipsekingdom.discordlink.sync.permission;

import com.eclipsekingdom.discordlink.sync.Troop;
import com.eclipsekingdom.discordlink.sync.TroopBank;
import com.eclipsekingdom.discordlink.sync.permission.listener.PexListener;
import ru.tehkode.permissions.PermissionManager;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Permission_Pex extends Permission {

    private static PermissionManager permissionManager = PermissionsEx.getPermissionManager();

    public Permission_Pex() {
        super(PermissionPlugin.PEX.getNamespace());
        new PexListener();
    }

    @Override
    public List<Troop> getTroops(UUID playerId) {
        List<Troop> troops = new ArrayList<>();
        for (Troop troop : TroopBank.getMinecraftSourced()) {
            if (inTroop(playerId, troop.getGroup())) {
                troops.add(troop);
            }
        }
        return troops;
    }

    @Override
    public boolean inTroop(UUID playerId, String troop) {
        PermissionUser user = permissionManager.getUser(playerId);
        return user.inGroup(troop);
    }


    @Override
    public boolean userExists(UUID playerId) {
        return permissionManager.getUser(playerId) != null;
    }

    @Override
    public void removeTroop(UUID playerId, String troop) {
        PermissionUser user = permissionManager.getUser(playerId);
        if (user.inGroup(troop)) {
            user.removeGroup(troop);
            user.save();
        }
    }

    @Override
    public void addTroop(UUID playerId, String troop) {
        PermissionUser user = permissionManager.getUser(playerId);
        if (!user.inGroup(troop)) {
            user.addGroup(troop);
            user.save();
        }
    }

    @Override
    public boolean groupExists(String groupName) {
        return PermissionsEx.getPermissionManager().getGroup(groupName) != null;
    }
}
