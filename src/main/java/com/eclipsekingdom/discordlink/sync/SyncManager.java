package com.eclipsekingdom.discordlink.sync;

import com.eclipsekingdom.discordlink.sync.discord.Discord;
import com.eclipsekingdom.discordlink.sync.permission.IPermission;
import com.eclipsekingdom.discordlink.util.scheduler.Scheduler;
import com.eclipsekingdom.discordlink.util.setup.SetupUtil;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class SyncManager {

    private static IPermission minecraft;
    private static Discord discord;
    private static Set<UUID> syncing = new HashSet<>();

    public SyncManager() {
        load();
    }

    private void load() {
        Scheduler.runTaskLaterAsync(() -> {
            for (UUID playerIDs : SetupUtil.getOnlinePlayers()) {
                coldSync(playerIDs);
            }
        }, 20 * 10);
    }

    public static void initialize(IPermission aPermission, Discord aDiscord) {
        minecraft = aPermission;
        discord = aDiscord;
    }

    public static void onJoin(UUID playerID) {
        Scheduler.runTaskLater(() -> {
            coldSync(playerID);
        }, 20 * 5);
    }

    public static void sync(UUID playerId, Server server) {
        troopProcess(playerId, () -> {
            syncServer(playerId, server);
        });
    }

    public static void strip(UUID playerId) {
        troopProcess(playerId, () -> {
            stripServer(playerId, Server.MINECRAFT);
            stripServer(playerId, Server.DISCORD);
        });
    }

    private static void syncServer(UUID playerId, Server from) {

        ITroopHolder fromHolder = from == Server.MINECRAFT ? minecraft : discord;
        ITroopHolder toHolder = from == Server.MINECRAFT ? discord : minecraft;
        Server to = from.getOther();

        Set<String> destinationTroops = new HashSet<>();
        if (fromHolder.userExists(playerId)) {
            for (Troop troop : fromHolder.getTroops(playerId)) {
                destinationTroops.add(troop.getHalf(to));
            }
        }

        if (toHolder.userExists(playerId)) {
            for (Troop troop : TroopBank.getTroops(from)) {
                String half = troop.getHalf(to);
                if (destinationTroops.contains(half)) {
                    if (!toHolder.inTroop(playerId, half)) {
                        toHolder.addTroop(playerId, half);
                    }
                } else {
                    if (toHolder.inTroop(playerId, half)) {
                        toHolder.removeTroop(playerId, half);
                    }
                }
            }
        }

        destinationTroops.clear();
    }


    private static void stripServer(UUID playerId, Server from) {
        Server to = from.getOther();
        ITroopHolder toStrip = to == Server.MINECRAFT ? minecraft : discord;
        if (toStrip.userExists(playerId)) {
            for (Troop troop : TroopBank.getTroops(from)) {
                String half = troop.getHalf(to);
                if (toStrip.inTroop(playerId, half)) {
                    toStrip.removeTroop(playerId, half);
                }
            }
        }
    }

    public static void coldSync(UUID playerId) {
        troopProcess(playerId, () -> {
            syncServer(playerId, Server.MINECRAFT);
            syncServer(playerId, Server.DISCORD);
        });
    }

    public static void troopProcess(UUID playerId, Runnable r) {
        if (!syncing.contains(playerId)) {
            syncing.add(playerId);
            Scheduler.runTaskLater(() -> {
                syncing.remove(playerId);
            }, 1 * 20);
            r.run();
        }
    }


}
