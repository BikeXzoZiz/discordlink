package com.eclipsekingdom.discordlink.sync;

import java.util.List;
import java.util.UUID;

public interface ITroopHolder {

    List<Troop> getTroops(UUID playerId);

    boolean userExists(UUID playerId);

    void addTroop(UUID playerId, String troop);

    void removeTroop(UUID playerId, String troop);

    boolean inTroop(UUID playerId, String troop);

}
