package com.eclipsekingdom.discordlink.sync;

public class Troop {

    private String groupID;
    private String roleID;
    private Server source;

    public Troop(String groupID, String roleID, Server source) {
        this.groupID = groupID;
        this.roleID = roleID;
        this.source = source;
    }

    public String getGroup() {
        return groupID;
    }

    public String getRole() {
        return roleID;
    }

    public Server getSource() {
        return source;
    }

    public String getHalf(Server server) {
        return server == Server.MINECRAFT ? groupID : roleID;
    }

}