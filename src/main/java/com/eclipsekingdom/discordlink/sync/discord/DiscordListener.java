package com.eclipsekingdom.discordlink.sync.discord;

import com.eclipsekingdom.discordlink.account.AccountCache;
import com.eclipsekingdom.discordlink.sync.Server;
import com.eclipsekingdom.discordlink.sync.SyncManager;
import com.eclipsekingdom.discordlink.util.setup.SetupUtil;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.guild.member.GuildMemberRoleAddEvent;
import net.dv8tion.jda.api.events.guild.member.GuildMemberRoleRemoveEvent;
import net.dv8tion.jda.api.events.role.RoleDeleteEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.util.Collection;
import java.util.UUID;

public class DiscordListener extends ListenerAdapter {

    public DiscordListener(JDA jda) {
        jda.addEventListener(this);
    }

    @Override
    public void onRoleDelete(RoleDeleteEvent e) {
        Collection<UUID> playerIds = SetupUtil.getOnlinePlayers();
        for (UUID playerID : playerIds) {
            SyncManager.sync(playerID, Server.DISCORD);
        }
        playerIds.clear();
        super.onRoleDelete(e);
    }

    @Override
    public void onGuildMemberRoleAdd(GuildMemberRoleAddEvent e) {
        processModify(e.getUser());
        super.onGuildMemberRoleAdd(e);
    }

    @Override
    public void onGuildMemberRoleRemove(GuildMemberRoleRemoveEvent e) {
        processModify(e.getUser());
        super.onGuildMemberRoleRemove(e);
    }

    private void processModify(User user) {
        UUID playerID = AccountCache.getPlayerID(user.getIdLong());
        if (playerID != null) {
            SyncManager.sync(playerID, Server.DISCORD);
        }
    }

}
