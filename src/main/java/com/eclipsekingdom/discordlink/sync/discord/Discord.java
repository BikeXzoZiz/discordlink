package com.eclipsekingdom.discordlink.sync.discord;

import com.eclipsekingdom.discordlink.account.AccountCache;
import com.eclipsekingdom.discordlink.sync.ITroopHolder;
import com.eclipsekingdom.discordlink.sync.Troop;
import com.eclipsekingdom.discordlink.sync.TroopBank;
import com.eclipsekingdom.discordlink.util.DiscordUtil;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;

import java.util.*;

public class Discord implements ITroopHolder {

    @Override
    public List<Troop> getTroops(UUID playerId) {
        List<Troop> troops = new ArrayList<>();
        Member member = getMember(playerId);
        Set<String> roleIDs = new HashSet<>();
        for (Role role : member.getRoles()) {
            roleIDs.add(role.getId());
        }
        for (Troop troop : TroopBank.getDiscordSourced()) {
            if (roleIDs.contains(troop.getRole())) {
                troops.add(troop);
            }
        }
        return troops;
    }

    @Override
    public boolean userExists(UUID playerId) {
        return getMember(playerId) != null;
    }

    private Member getMember(UUID playerId) {
        try {
            if (AccountCache.hasDiscord(playerId)) {
                return DiscordUtil.getMember(AccountCache.getDiscordID(playerId));
            }
        } catch (Exception e) {
        }
        return null;
    }

    public void addTroop(UUID playerId, String troop) {
        Member member = getMember(playerId);
        Role role = DiscordUtil.getRole(troop);
        if (role != null) {
            List<Role> roles = member.getRoles();
            if (!roles.contains(role)) {
                DiscordUtil.addRole(member, role);
            }
        }
    }

    @Override
    public void removeTroop(UUID playerId, String troop) {
        Member member = getMember(playerId);
        Role role = DiscordUtil.getRole(troop);
        if (role != null) {
            List<Role> roles = member.getRoles();
            if (roles.contains(role)) {
                DiscordUtil.removeRole(member, role);
            }
        }
    }

    @Override
    public boolean inTroop(UUID playerId, String troop) {
        Member member = getMember(playerId);
        for (Role role : member.getRoles()) {
            if (role.getId().equals(troop)) {
                return true;
            }
        }
        return false;
    }

}
