package com.eclipsekingdom.discordlink.sync;

public enum Server {
    MINECRAFT, DISCORD,
    ;

    public Server getOther() {
        return this == MINECRAFT ? DISCORD : MINECRAFT;
    }
}
