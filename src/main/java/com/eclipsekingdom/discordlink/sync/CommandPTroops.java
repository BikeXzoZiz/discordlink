package com.eclipsekingdom.discordlink.sync;

import com.eclipsekingdom.discordlink.DiscordLink;
import com.eclipsekingdom.discordlink.sync.permission.IPermission;
import com.eclipsekingdom.discordlink.util.Amount;
import com.eclipsekingdom.discordlink.util.DiscordUtil;
import com.eclipsekingdom.discordlink.util.InfoList;
import com.eclipsekingdom.discordlink.util.setup.BungeeUtil;
import com.eclipsekingdom.discordlink.util.system.Permissions;
import net.dv8tion.jda.api.entities.Role;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

import java.util.ArrayList;
import java.util.List;

import static com.eclipsekingdom.discordlink.util.language.Message.WARN_NOT_PERMITTED;

public class CommandPTroops extends Command {


    private IPermission permission;

    public CommandPTroops(String name) {
        super(name);
        this.permission = DiscordLink.getPermission();
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (Permissions.canListTroops(sender)) {
            processList(sender, args);
        } else {
            BungeeUtil.sendMessage(sender, ChatColor.RED + WARN_NOT_PERMITTED.toString());
        }
    }


    public void processList(CommandSender sender, String[] args) {
        List<String> items = new ArrayList<>();
        for (Troop troop : TroopBank.getTroops()) {
            boolean groupExists = permission.groupExists(troop.getGroup());
            Role role = DiscordUtil.getRole(troop.getRole());
            boolean roleExists = role != null;
            String groupString = groupExists ? ChatColor.GRAY + troop.getGroup() : ChatColor.RED + troop.getGroup();
            String roleString = roleExists ? ChatColor.GRAY + role.getName() : ChatColor.RED + troop.getRole();
            String connector = getLinkString(troop, groupExists && roleExists);
            items.add(groupString + connector + roleString);
        }
        InfoList infoList = new InfoList(ChatColor.GOLD + "Troops:", items, 7, "troops");
        int page = args.length > 0 ? Amount.parse(args[0]) : 1;
        infoList.displayTo(sender, page);
    }

    public String getLinkString(Troop troop, boolean linked) {
        if (linked) {
            return troop.getSource() == Server.MINECRAFT ? ChatColor.GREEN + " ==> " : ChatColor.GREEN + " <== ";
        } else {
            return troop.getSource() == Server.MINECRAFT ? ChatColor.GRAY + " =/=> " : ChatColor.GRAY + " <=/= ";
        }
    }

}
