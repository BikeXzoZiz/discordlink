package com.eclipsekingdom.discordlink.sync;

import com.eclipsekingdom.discordlink.DiscordLink;
import com.eclipsekingdom.discordlink.sync.permission.IPermission;
import com.eclipsekingdom.discordlink.util.Amount;
import com.eclipsekingdom.discordlink.util.DiscordUtil;
import com.eclipsekingdom.discordlink.util.InfoList;
import com.eclipsekingdom.discordlink.util.system.Permissions;
import net.dv8tion.jda.api.entities.Role;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;

import static com.eclipsekingdom.discordlink.util.language.Message.WARN_NOT_PERMITTED;

public class CommandSTroops implements CommandExecutor {

    private IPermission permission;

    public CommandSTroops() {
        this.permission = DiscordLink.getPermission();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (Permissions.canListTroops(sender)) {
            processList(sender, args);
        } else {
            sender.sendMessage(ChatColor.RED + WARN_NOT_PERMITTED.toString());
        }
        return false;
    }

    public void processList(CommandSender sender, String[] args) {
        List<String> items = new ArrayList<>();
        for (Troop troop : TroopBank.getTroops()) {
            boolean groupExists = permission.groupExists(troop.getGroup());
            Role role = DiscordUtil.getRole(troop.getRole());
            boolean roleExists = role != null;
            String groupString = groupExists ? ChatColor.GRAY + troop.getGroup() : ChatColor.RED + troop.getGroup();
            String roleString = roleExists ? ChatColor.GRAY + role.getName() : ChatColor.RED + troop.getRole();
            String connector = getLinkString(troop, groupExists && roleExists);
            items.add(groupString + connector + roleString);
        }
        InfoList infoList = new InfoList(ChatColor.GOLD + "Troops:", items, 7, "troops");
        int page = args.length > 0 ? Amount.parse(args[0]) : 1;
        infoList.displayTo(sender, page);
    }

    public String getLinkString(Troop troop, boolean linked) {
        if (linked) {
            return troop.getSource() == Server.MINECRAFT ? ChatColor.GREEN + " ==> " : ChatColor.GREEN + " <== ";
        } else {
            return troop.getSource() == Server.MINECRAFT ? ChatColor.GRAY + " =/=> " : ChatColor.GRAY + " <=/= ";
        }
    }

}
