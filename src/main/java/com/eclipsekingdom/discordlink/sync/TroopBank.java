package com.eclipsekingdom.discordlink.sync;

import com.eclipsekingdom.discordlink.util.system.PluginConfig;

import java.util.ArrayList;
import java.util.List;

public class TroopBank {

    private static List<Troop> discordSourced = new ArrayList<>();
    private static List<Troop> minecraftSourced = new ArrayList<>();
    private static List<Troop> troops = new ArrayList<>();

    public TroopBank() {
        load();
    }

    private void load() {
        for (Troop troop : PluginConfig.getTroops()) {
            troops.add(troop);
            if (troop.getSource() == Server.MINECRAFT) {
                minecraftSourced.add(troop);
            } else {
                discordSourced.add(troop);
            }
        }
    }

    public static List<Troop> getDiscordSourced() {
        return discordSourced;
    }

    public static List<Troop> getMinecraftSourced() {
        return minecraftSourced;
    }

    public static List<Troop> getTroops() {
        return troops;
    }

    public static List<Troop> getTroops(Server server) {
        return server == Server.MINECRAFT ? minecraftSourced : discordSourced;
    }

}
